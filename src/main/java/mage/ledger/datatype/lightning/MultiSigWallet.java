/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package mage.ledger.datatype.lightning;

import java.io.Serializable;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import mage.ledger.datatype.Address;
import mage.ledger.datatype.UUIDAddress;

/**
 * A wallet stores the addresses which can be used to receive or spend a
 * cryptocurrency designated by the parameter key.
 *
 * The {@link MultiSigWallet} allows several agents to share a common wallet.
 * 
 * @author Önder Gürcan
 * @author Mohamed Aimen Djari
 *
 */
public class MultiSigWallet implements IChannelDeliverer<Address>, Serializable {

	/**
	 * Generated serial version UID.
	 */
	private static final long serialVersionUID = -2642380196989435796L;

	/** Store addresses by cryptocurrency's name. */
	private Hashtable<String, Address> wallet;

	/** Store the wallet's owners. */
	private List<Address> owners = new Vector<Address>();

	/** Stores the balances of the owners of this shared wallet. */
	public Hashtable<String, Double> balances = new Hashtable<String, Double>();

	/** The address of the wallet. */
	private String walletAddress;

	/** Tells if the channel is open or not. */
	private boolean isOpen;

	/** The list of observers to notify. */
	private Hashtable<String, IChannelObserver> observers = new Hashtable<String, IChannelObserver>();

	/**
	 * Creates a new {@link MultiSigWallet} instance.
	 *
	 * The created wallet has a BTC address and is open.
	 *
	 * @param address1 address of the first participant
	 * @param address2 address of the second participant
	 */
	public MultiSigWallet(Address address1, Address address2) {
		if (!this.owners.contains(address1)) {
			this.owners.add(address1);
		}
		
		if (!this.owners.contains(address2)) {
			this.owners.add(address2);
		}
		
		if (address1.toString().compareTo(address2.toString()) < 0) {
			this.walletAddress = address1.toString().substring(0, (address1.toString().length() / 2))
					+ address2.toString().substring(address2.toString().length() / 2, address2.toString().length());
		} else {
			this.walletAddress = address2.toString().substring(0, (address2.toString().length() / 2))
					+ address1.toString().substring(address1.toString().length() / 2, address1.toString().length());

		}

		this.wallet = new Hashtable<String, Address>();
		
		addAddress("BTC", new UUIDAddress(walletAddress));
		
		isOpen = true;
		
		this.setBalances();
	}

	/**
	 * Sets the balances of each participant.
	 */
	private void setBalances() {
		Double balance = balances.get(owners.get(0).toString());
		if (balance == null) {
			balances.put(owners.get(0).toString(), (double) 0);
		}

		balance = balances.get(owners.get(1).toString());
		if (balance == null) {
			balances.put(owners.get(1).toString(), (double) 0);
		}
	}

	/**
	 * Resets the balance of the provided participant.
	 *
	 * @param address address of the participant
	 */
	public void resetBalanceOf(Address address) {
		Double balance = balances.get(address.toString());
		if (balance != null) {
			balances.put(address.toString(), 0.0);
		}
		notifyChannelObserver(null);
	}

	/**
	 * Adds a new address to this wallet, stored using the provided key.
	 *
	 * If a previous address was registered under the provided key, it is replaced by the new address.
	 *
	 * @param key crypto's name
	 * @param address wallet's address
	 */
	public void addAddress(String key, Address address) {
		this.wallet.put(key, address);
	}

	/**
	 * Removes an address from the wallet.
	 *
	 * @param key crypto's name associated to the address
	 */
	public void removeAddress(String key) {
		this.wallet.remove(key);
	}

	/**
	 * Gets the address associated to the provided key.
	 *
	 * @param key crypto's name
	 * @return the address if it exists or {@code null}
	 */
	public Address getAddress(String key) {
		return this.wallet.get(key);
	}

	/**
	 * Gets this wallet's size.
	 *
	 * @return a positive integer
	 */
	public int size() {
		return this.wallet.size();
	}

	/**
	 * Gets the list of participants.
	 *
	 * @return a list with two elements
	 */
	public List<Address> getParticipants() {
		return owners;
	}

	/**
	 * Tells if the wallet is open or closed.
	 *
	 * @return {@code true} if the wallet is open.
	 */
	public boolean isOpen() {
		return isOpen;
	}

	/**
	 * Sets the open status of the wallet.
	 *
	 * @param isOpen the new open value
	 */
	public void setOpen(boolean isOpen) {
		this.isOpen = isOpen;
		notifyChannelObserver(null);
	}

	/**
	 * Given a participant, gets its balance.
	 *
	 * @param address the participant's address
	 * @return the balance or {@code null} if the participant doesn't own the wallet
	 */
	public Double getBalanceOf(Address address) {
		return balances.get(address.toString());
	}

	/**
	 * Sets the balance of the participant.
	 *
	 * Only works if the address belongs to a participant owning the wallet.
	 *
	 * @param address participant's address
	 * @param payload the new balance
	 */
	public void setBalanceOf(String address, Double payload) {
		Double balance = balances.get(address);
		if (balance != null) {
			balances.put(address, payload + balance);
		}
		notifyChannelObserver(null);
	}

	/**
	 * Sends a lightning transaction from {@code sender} to {@code receiver}.
	 *
	 * @param sender address of the participant sending the transaction
	 * @param receiver address of the participant receiving the money
	 * @param payload how much must be transferred
	 * @throws IllegalArgumentException the sender's deposit in the wallet is not sufficient to cover
	 * 		the transaction.
	 */
	public void sendLightningTransaction(Address sender, Address receiver, Double payload) {
		Double balanceSender = balances.get(sender.toString());
		if (balanceSender >= payload) {
			Double newBalanceReceiver = balances.get(receiver.toString()) + payload;
			balances.put(receiver.toString(), newBalanceReceiver);
			double newBalanceSender = balanceSender - payload;
			balances.put(sender.toString(), newBalanceSender);
		} else {
			throw new IllegalArgumentException("Please put deposit first !");
		}
		notifyChannelObserver(null);
	}

	@Override
	public void register(Address address, IChannelObserver obj) {
		if (obj == null) {
			throw new IllegalArgumentException("Null Observer");
		}
		observers.put(address.toString(), obj);
		notifyChannelObserver(null);
	}

	@Override
	public void unregister(Address address) {
		observers.remove(address.toString());
	}

	/**
	 * Notifies the corresponding message observer if it exists and registered to
	 * the list of observers.
	 * 
	 * Since it is not possible to unregister an observer using a probe (because
	 * when removing() is called, the address of the agent is already null), we
	 * unregister here.
	 *
	 * @param add ignored
	 */
	@Override
	public void notifyChannelObserver(Address add) {
		Iterator<Address> iterator = getParticipants().iterator();
		while (iterator.hasNext()) {
			Address address = iterator.next();
			IChannelObserver messageObserver = this.observers.get(address.toString());
			if (messageObserver != null) {
				messageObserver.onChannelUpdate();
			}
		}
	}

}
