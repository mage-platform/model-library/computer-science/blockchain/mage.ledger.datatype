/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package mage.ledger.datatype.lightning;

import java.io.Serializable;

/**
 * Observer pattern for channels. Observer part.
 *
 * @see IChannelDeliverer
 * 
 * @author Önder Gürcan
 * @author Aimen Djari
 */
public interface IChannelObserver extends Serializable {
	
	/**
	 * Updates the observer, used by IMessageDeliverer.
	 */
	public void onChannelUpdate();

}
