/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package mage.ledger.datatype.lightning;

import java.io.Serializable;

/**
 * Observer pattern for channels. Observable part.
 *
 * @param <A> address type
 * @see IChannelObserver
 * 
 * @author Önder Gürcan
 * @author Aimen Djari
 */
public interface IChannelDeliverer<A> extends Serializable {

	/**
	 * Registers a given observer with its address.
	 *
	 * @param address observer's address
	 * @param obj the observer
	 */
	public void register(A address, IChannelObserver obj);
	
	/**
	 * Unregisters an observer using its address.
	 *
	 * @param address observer's address
	 */
	public void unregister(A address);
	
	/**
	 * Notifies the observers of message delivery.
	 *
	 * @param address the observer's address
	 */
	public void notifyChannelObserver(A address);
	
}

