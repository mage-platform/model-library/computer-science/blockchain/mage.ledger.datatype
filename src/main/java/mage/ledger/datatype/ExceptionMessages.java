/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package mage.ledger.datatype;

import java.io.Serializable;

/**
 * A collection of error messages used in the module.
 *
 * @author Önder Gürcan
 */
public class ExceptionMessages implements Serializable {

	/**
	 * Generated serial version UID. 
	 */
	private static final long serialVersionUID = 5275144830835389609L;
	
	public static final String THIS_LINE_SHOULD_NOT_BE_EXECUTED = "This line should not be executed.";
	public static final String FEE_SHOULD_BE_HIGHER_THAN_ZERO = "Total output should be less than total input.";
	public static final String INPUT_OR_OUTPUT_SHOULD_BE_POSITIVE_VALUE = "Input or output should be a positive value.";
	public static final String BLOCK_NUMBER_SHOULD_BE_HIGHER_THAN_OR_EQUAL_TO_ZERO = "Block number should be higher than zero.";
	public static final String INVALID_HASH_VALUE = "Invalid hash value.";
	public static final String TIME_STAMP_SHOULD_BE_HIGHER_THAN_OR_EQUAL_TO_ZERO = "Timestamp should be higher than or equal to zero.";
	public static final String NONCE_SHOULD_BE_HIGHER_THAN_OR_EQUAL_TO_ZERO = "Nonce should be higher than or equal to zero.";
	public static final String GENESIS_BLOCK_CANNOT_BE_NULL = "Genesis block cannot be null.";
	public static final String RECEIVER_ADDRESS_CANNOT_BE_NULL = "The receiver address cannot be null.";
	public static final String GENESIS_BLOCK_BLOCK_NUMBER_SHOULD_BE_EQUAL_ZERO = "Genesis block block number should be equal to zero.";
	public static final String GENESIS_BLOCK_SHOULD_NOT_HAVE_A_PREVIOUS_BLOCK = "Genesis block shoul not have a previous block.";
	public static final String BLOCKCHAIN_IS_NOT_STABLE = "Blockchain is not stable.";
	public static final String LOCK_TIME_MUST_BE_POSITIVE = "The lock time must be positive.";
}
