/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package mage.ledger.datatype;

import java.io.Serializable;
import java.util.Objects;

/**
 * Some kind of payload, storing a non-null value.
 * 
 * @author Önder Gürcan
 */
public class Payload implements Serializable {

    // Variables.

    /**
	 * Generated serial version UID. 
	 */
	private static final long serialVersionUID = -4303094685000863962L;
	
	/** THe stored value. */
    private final Object value;

    // Constructors.

    /**
     * Creates a new {@link Payload instance}.
     *
     * @param value content of the payload
     */
    public Payload(Object value) {
    	if (value != null) {
    		this.value = value;
    	} else {
    		throw new NullPointerException("Payload value cannot be null.");
    	}        
    }

    // Methods.

    /**
     * Gets the stored value.
     *
     * @return the data of the payload
     * @throws java.util.NoSuchElementException if content is {@code null}
     */
    public Object getValue() {
        return this.value;
    }

    /**
     * Tells if the stored value is numerical or not.
     *
     * @return true if {@link #value} is a subclass of {@link Number}, else false.
     * @throws java.util.NoSuchElementException if content is {@code null}
     */
    public boolean isNumerical() {
        return this.value instanceof Number;
    }

    /**
     * Get the payload content as a {@link Number}.
     *
     * @return either the numerical value or 0 if the content is not numerical
     * @throws java.util.NoSuchElementException if content is {@code null}
     */
    public double getNumericalValue() {
        if (this.isNumerical()) {
            return ((Number) this.value).doubleValue();
        } else {
            return 0.d;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Payload)) return false;
        Payload payload = (Payload) o;
        return value.equals(payload.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }

    @Override
    public String toString() {
        return this.value.toString();
    }
}
