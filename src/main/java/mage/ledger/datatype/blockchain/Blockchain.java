/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package mage.ledger.datatype.blockchain;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import mage.ledger.datatype.Address;
import mage.ledger.datatype.ExceptionMessages;
import mage.ledger.datatype.Transaction;

/**
 * Java representation of a simple blockchain.
 *
 * <p>It is only a wrapper class around a simple {@link List} but providing blockchain related
 * operations like finding a block by hashcode, getting the head block ...
 *
 * @param <Tx> Type of transactions.
 * @param <T> Timestamp type of blocks.
 * 
 * @author Önder Gürcan
 * @author Aimen Djari
 * @author Guillaume Rakotomalala
 */
public class Blockchain<Tx extends Transaction, T extends Number> implements Iterable<Block<Tx, T>>, Serializable {

    /**
	 * Generated serial version UID. 
	 */
	private static final long serialVersionUID = 8581582696488556229L;
	
	/** The list of blocks. */
    private List<Block<Tx, T>> blockchain = new Vector<>();

    /**
     * Creates a new {@link Blockchain} instance using the provided genesis block.
     *
     * @param genesisBlock the genesis block
     * @throws IllegalArgumentException genesis block is {@code null}, its number is not 0, or its
     *      parent hash is not 0
     */
    public Blockchain(Block<Tx, T> genesisBlock) {
        if (genesisBlock == null) {
            throw new IllegalArgumentException(ExceptionMessages.GENESIS_BLOCK_CANNOT_BE_NULL);
        } else if (genesisBlock.getBlockNumber() != 0) {
            throw new IllegalArgumentException(ExceptionMessages.GENESIS_BLOCK_BLOCK_NUMBER_SHOULD_BE_EQUAL_ZERO);
        } else if (genesisBlock.getPreviousBlockHashCode() != 0) {
            throw new IllegalArgumentException(ExceptionMessages.GENESIS_BLOCK_SHOULD_NOT_HAVE_A_PREVIOUS_BLOCK);
        } else {
            this.blockchain.add(genesisBlock);
        }
    }

    /**
     * Gets a block using its number.
     *
     * @param blockNumber number of the block
     * @return Either the block instance or {@code null} if the block is not in the blockchain.
     */
    public Block<Tx, T> getBlockByNumber(int blockNumber) {
        if (blockchain.size() <= blockNumber) {
            return null;
        } else {
            return blockchain.get(blockNumber);
        }
    }

    /**
     * Gets a block using its hash.
     *
     * @param hashCode hash of the desired block
     * @return either the block instance or {@code null} if the block is not ine the blockchain
     */
    public Block<Tx, T> getBlockByHashcode(int hashCode) {
        Block<Tx, T> result = null;
        Block<Tx, T> block;
        for (int i = 0; i < blockchain.size(); i++) {
            if ((block = blockchain.get(i)).hashCode() == hashCode) {
                result = block;
            }
        }
        return result;
    }

    /**
     * Gets a transaction using its hash.
     *
     * The method loops on each block in the blockchain and try to find a matching transaction.
     *
     * @param hashCode transaction's hash
     * @return either the transaction instance or {@code null} if no such transaction is found in
     *      the blockchain
     */
    public Tx getTransactionByHashcode(int hashCode) {
        int size = blockchain.size();
        if (size > 0) {
            for (int i = 0; i < blockchain.size(); i++) {
                Block<Tx, T> block = blockchain.get(i);
                if (block.getTransaction(hashCode) != null) {
                    return block.getTransaction(hashCode);
                }
            }
        }
        return null;
    }

    /**
     * Gets the blockchain's head block.
     *
     * @return the head block (which always exists)
     */
    public Block<Tx, T> getHeadBlock() {
        return blockchain.get(blockchain.size() - 1);
    }

    /**
     * Appends the given block in the blockchain.
     *
     * @param block block to append
     * @return {@code true} if the block is added, {@code false} otherwise
     */
    public boolean append(Block<Tx, T> block) {
        boolean result = false;

        if (!(blockchain.contains(block))) {
            result = blockchain.add(block);
        }

        return result;
    }

    /**
     * Tells if the given block or transaction hash matches with a block or a transaction in the
     * blockchain.
     *
     * @param hashCode hash of the block or transaction
     * @return {@code true} if the blockchain contains the block or the transaction, {@code false}
     *      otherwise.
     */
    public boolean contains(Integer hashCode) {
        boolean hasBlock = getBlockByHashcode(hashCode) != null;
        boolean hasTransaction = getTransactionByHashcode(hashCode) != null;
        return hasBlock || hasTransaction;
    }

    /**
     * Tells how many transactions this blockchain contains.
     *
     * @return a positive integer
     */
    public Integer getNumberOfTransactions() {
        Integer result = 0;
        for (int i = 0; i < blockchain.size(); i++) {
            Block<Tx, T> block = blockchain.get(i);
            result += block.getTransactions().size();
        }

        return result;
    }

    /**
     * Tells how many blocks this blockchain contains.
     *
     * @return a positive integer
     */
    public int size() {
        return blockchain.size();
    }

    @Override
    public Iterator<Block<Tx, T>> iterator() {
        return this.blockchain.iterator();
    }

    /**
     * Clear the blockchain by removing all blocks, including the genesis block.
     */
    public void clear() {
        blockchain.clear();
    }

    /**
     * Tells if the blockchain contains the provided block.
     *
     * @param block the block
     * @return {@code true} if the blockchain contains the block, {@code false} otherwise
     */
    public boolean contains(Block<Tx, T> block) {
        boolean hasBlock = false;
        for (Block<Tx, T> txTBlock : blockchain) {
            if (txTBlock == block) {
                hasBlock = true;
            }
        }
        return hasBlock;
    }

    /**
     * Given a transactions, retrieves the block that stores it.
     *
     * @param transaction the transaction
     * @return The block instance or {@code null} if the transaction is not stored in the blockchain
     */
    public Block<Tx, T> getContainingBlock(Tx transaction) {
        Block<Tx, T> block = null;
        for (Block<Tx, T> txTBlock : blockchain) {
            if (txTBlock.getTransactions().contains(transaction)) {
                block = txTBlock;
            }
        }
        return block;
    }

    /**
     * Returns all the transactions that are sent to the given address.
     *
     * @param address the address
     * @return a possibly empty list of transactions, but never {@code null}
     */
    public List<Tx> getReceivedTransactionsOf(Address address) {
        List<Tx> receivedTransactions = new Vector<>();

        for (Block<Tx, T> block : blockchain) {
            for (Tx transaction : block.getTransactions()) {
                if ((transaction.getReceiver().toString().equals(address.toString()))
                        && (transaction.getSender() != null)) {
                    receivedTransactions.add(transaction);
                }
            }
        }
        return receivedTransactions;
    }

    @Override
    public boolean equals(Object obj) {
        boolean equals = false;
        if (obj != null && obj instanceof Blockchain) {
            Blockchain<Tx, T> blockchain = (Blockchain<Tx, T>) obj;
            int height = blockchain.getHeadBlock().getBlockNumber();
            int refHeight = this.getHeadBlock().getBlockNumber();
            if (height == refHeight) {
                equals = true;
                for (int i = 0; i <= height; i++) {
                    equals &= blockchain.getBlockByNumber(i).equals(this.getBlockByNumber(i));

                }
            }
        }
        return equals;
    }

}
