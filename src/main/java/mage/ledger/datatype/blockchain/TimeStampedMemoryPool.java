/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package mage.ledger.datatype.blockchain;

import java.io.Serializable;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import mage.ledger.datatype.Transaction;

/**
 * Associates transactions to timestamps.
 *
 * <p>This object is meant to be used in conjunction with {@link MemoryPool} in order to store
 * transactions receiving date.
 *
 * @param <T> time's type
 * 
 * @author Önder Gürcan
 * @author Aimen Djari
 */
public class TimeStampedMemoryPool<T extends Number> implements Serializable  {

	/**
	 * Generated serial version UID.
	 */
	private static final long serialVersionUID = 7695991782723128934L;
	
	/**
	 * Used for keeping tuples (hashcode of the transaction, the reception time of
	 * transaction).
	 */
	private Hashtable<Integer, T> memoryPool;

	/**
	 * Creates a new empty {@link TimeStampedMemoryPool} instance.
	 */
	public TimeStampedMemoryPool() {
		this.memoryPool = new Hashtable<Integer, T>();
	}

	/**
	 * Associates the given transaction to the provided timestamp.
	 *
	 * @param transaction the transaction
	 * @param timestamp the timestamp
	 */
	public void put(Transaction transaction, T timestamp) {

		this.memoryPool.put(transaction.hashCode(), timestamp);
	}

	/**
	 * Gets an {@link Enumeration} on transactions' hashes.
	 *
	 * @return an enumeration of hashes
	 */
	public Enumeration<Integer> keys() {
		return this.memoryPool.keys();
	}

	/**
	 * Gets transactions' hashes as a list.
	 *
	 * @see #keys()
	 * @return a possibly empty list of hashes, but never {@code null}
	 */
	public List<Integer> getKeysAsList() {
		List<Integer> keys = new Vector<Integer>();
		Enumeration<Integer> poolKeys = this.memoryPool.keys();
		while (poolKeys.hasMoreElements()) {
			Integer nextElement = poolKeys.nextElement();
			keys.add(nextElement);
		}
		return keys;
	}

	/**
	 * Gets the collection of stored timestamps.
	 *
	 * @return a possibly empty collection of timestamps, but never {@code null}.
	 */
	public Collection<T> values() {
		return this.memoryPool.values();
	}

	/**
	 * Checks if the given hash is stored in this pool.
	 *
	 * @param hashCode hash to test
	 * @return {@code true} if the hash is stored in the pool, {@code false} otherwise
	 */
	public boolean containsHashcode(Integer hashCode) {
		return this.memoryPool.containsKey(hashCode);
	}

	/**
	 * Gets the timestamp associated to the provided hash.
	 *
	 * @param hashCode the hash
	 * @return the timestamp, or {@code null} if no timestamp is found
	 */
	public T get(Integer hashCode) {
		return this.memoryPool.get(hashCode);
	}

	/**
	 * Remove the timestamp associated to the provided hash.
	 *
	 * @param hashCode the hash
	 */
	public void remove(Integer hashCode) {
		this.memoryPool.remove(hashCode);
	}

	/**
	 * Gets the size of the pool.
	 *
	 * @return a positive integer
	 */
	public int size() {
		return this.memoryPool.values().size();
	}

	@Override
	public String toString() {
		String result = "[\n";
		Iterator<T> iterator = memoryPool.values().iterator();
		while (iterator.hasNext()) {
			T timestamp = iterator.next();
			result += "\t " + timestamp.toString() + "\n";
		}
		result += " ]";
		return result;
	}

}
