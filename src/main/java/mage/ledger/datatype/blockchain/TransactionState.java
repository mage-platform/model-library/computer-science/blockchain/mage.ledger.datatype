/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package mage.ledger.datatype.blockchain;

import mage.ledger.datatype.Transaction;

import java.io.Serializable;

/**
 * The transaction and its state which is defined by its creation time,
 * confirmation Time and confirming Block.
 *
 * @author Önder Gürcan
 * @author Aimen Djari
 * @author Guillaume Rakotomalala
 */
public class TransactionState implements Serializable {

    /**
	 * Generated serial version UID.
	 */
	private static final long serialVersionUID = 8307461501542986647L;

	/**
     * The transaction
     */
    private Transaction transaction;

    /**
     * the creation time of the transaction, if received, creation time = N/A
     */
    private String creationTime;

    /**
     * The confirmation block of the transaction which is the block containing the
     * transaction
     */
    private String confirmingBlock;

    /**
     * The confirmation time of the transaction which is the timestamp of the
     * confirming block
     */
    private String confirmationTime;

    /**
     * Default constructor.
     *
     * @param transaction the transaction
     * @param creationTime the creation time
     */
    public TransactionState(Transaction transaction, String creationTime) {
        this.transaction = transaction;
        this.creationTime = creationTime;
    }

    /**
     * Gets the stored transaction.
     *
     * @return the transaction
     */
    public Transaction getTransaction() {
        return this.transaction;
    }

    /**
     * Gets the creation time.
     *
     * @return the creation time
     */
    public String getCreationTime() {
        return this.creationTime;
    }

    /**
     * Gets the confirmation time.
     *
     * @return confirmation time
     */
    public String getConfirmationTime() {
        return confirmationTime;
    }

    /**
     * Sets the confirmation time.
     *
     * @param confirmationTime the new confirmation time
     */
    public void setConfirmationTime(String confirmationTime) {
        this.confirmationTime = confirmationTime;
    }

    /**
     * Gets the confirming block.
     *
     * @return the confirming block
     */
    public String getConfirmingBlock() {
        return confirmingBlock;
    }

    /**
     * Sets the confirming block.
     *
     * @param confirmationBlock the confirming block
     */
    public void setConfirmingBlock(String confirmationBlock) {
        this.confirmingBlock = confirmationBlock;
    }

}
