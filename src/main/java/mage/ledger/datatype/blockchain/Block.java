/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package mage.ledger.datatype.blockchain;

import java.io.Serializable;
import java.util.List;
import java.util.Vector;

import mage.ledger.datatype.ExceptionMessages;
import mage.ledger.datatype.Transaction;

/**
 * Java representation of a blockchain block.
 *
 * Each block:
 * <ul>
 * <li>Has a block number (greater or equals to 0).</li>
 * <li>Knows its parent block hash.</li>
 * <li>Is timestamped.</li>
 * <li>Has a nonce.</li>
 * <li>Contains a set of transactions.</li>
 * </ul>
 *
 * @param <Tx> Type of transactions.
 * @param <T>  Timestamp type of blocks.
 * 
 * @author Önder Gürcan
 * @author Aimen Djari
 * @author Guillaume Rakotomalala
 */
public class Block<Tx extends Transaction, T extends Number> implements Serializable {

	/**
	 * Generated serial version UID.
	 */
	private static final long serialVersionUID = 7395020092406309900L;

	/** The block number, greater or equal to 0. */
	private Integer blockNumber;

	/** The hash of the parent block. */
	private Integer previousBlockHashCode;

	/** When this block has been created. */
	private T timeStamp;

	/** The nonce for the block. */
	private Integer nonce;

	/** The set of transactions stored as a {@link MerkleTree}. */
	private MerkleTree<Tx> transactions = new MerkleTree<>();

	/** The coinbase transaction. */
	private Tx coinbaseTransaction;

	/** A list of coinbase transactions. */
	private List<Tx> listCoinbaseTransaction = new Vector<>();

	/**
	 * Creates a new {@link Block} instance.
	 *
	 * Nonce is set to 0 and the block has no transactions.
	 *
	 * @param blockNumber           this block's number
	 * @param previousBlockHashCode parent hash
	 * @param timeStamp             creation time
	 * @throws IllegalArgumentException both {@code blockNumber} and
	 *                                  {@code timeStamp} must be greater or equal
	 *                                  to 0
	 */
	public Block(Integer blockNumber, Integer previousBlockHashCode, T timeStamp) {
		if (blockNumber >= 0) {
			this.blockNumber = blockNumber;
		} else {
			throw new IllegalArgumentException(ExceptionMessages.BLOCK_NUMBER_SHOULD_BE_HIGHER_THAN_OR_EQUAL_TO_ZERO);
		}

		// if (previousBlockHashCode != 0) {
		this.previousBlockHashCode = previousBlockHashCode;
		// } else {
		// throw new IllegalArgumentException(ExceptionMessages.INVALID_HASH_VALUE);
		// }

		if (timeStamp.longValue() >= 0L) {
			this.timeStamp = timeStamp;
		} else {
			throw new IllegalArgumentException(ExceptionMessages.TIME_STAMP_SHOULD_BE_HIGHER_THAN_OR_EQUAL_TO_ZERO);
		}

		this.setNonce(0);
	}

	/**
	 * Get his block's number.
	 *
	 * @return a positive integer
	 */
	public Integer getBlockNumber() {
		return this.blockNumber;
	}

	/**
	 * Get the parents hash.
	 *
	 * @return hash
	 */
	public int getPreviousBlockHashCode() {
		return this.previousBlockHashCode;
	}

	/**
	 * Get his block's creation time.
	 *
	 * @return positive number
	 */
	public T getTimeStamp() {
		return timeStamp;
	}

	/**
	 * Set this block's nonce to the provided value.
	 *
	 * @param nonce the new nonce
	 * @throws IllegalArgumentException the {@code nonce} must be greater or equal
	 *                                  to 0
	 */
	public void setNonce(int nonce) {
		if (nonce >= 0) {
			this.nonce = nonce;
		} else {
			throw new IllegalArgumentException(ExceptionMessages.NONCE_SHOULD_BE_HIGHER_THAN_OR_EQUAL_TO_ZERO);
		}
	}

	/**
	 * Get this block's nonce.
	 *
	 * @return a positive integer
	 */
	public Integer getNonce() {
		return this.nonce;
	}

	/**
	 * Adds the given transaction in the internal merkle tree.
	 *
	 * If either the sender or the receiver is {@code null} then the transaction is
	 * ignored.
	 *
	 * @param transaction transaction to add
	 */
	private void addTransaction(Tx transaction) {
		if (transaction.getSender() != null && transaction.getReceiver() != null) {
			this.transactions.addHashedElement(transaction);
		}
	}

	/**
	 * Given its hash, gets a stored transaction.
	 * 
	 * @param hashCode hash of the transaction
	 * @return Either the transaction or {@code null} if the transaction is not
	 *         stored in the block.
	 */
	public Tx getTransaction(int hashCode) {
		return transactions.getHashedElement(hashCode);
	}

	/**
	 * Gets the coinbase transaction.
	 *
	 * @return the coinbase transaction or {@code null} if this block has no such
	 *         transaction.
	 */
	public Tx getCoinbaseTransaction() {
		return this.coinbaseTransaction;
	}

	/**
	 * Gets the list of coinbase transactions.
	 *
	 * @return a possibly empty list, but never {@code null}.
	 */
	public List<Tx> getListCoinbaseTransaction() {
		return this.listCoinbaseTransaction;
	}

	/**
	 * Adds the provided transactions into this block's transactions.
	 *
	 * Each transaction is added using {@link #addTransaction(Transaction)}.
	 * 
	 * @param transactionList transactions to add
	 * @see #addTransaction(Transaction)
	 */
	public void addTransactions(List<Tx> transactionList) {
		synchronized (transactionList) {
			for (Tx transaction : transactionList) {
				addTransaction(transaction);
			}
		}

//		Iterator<Tx> iterator = transactionList.iterator();
//		while (iterator.hasNext()) {
//			Tx transaction = iterator.next();
//			addTransaction(transaction);
//		}
	}

	/**
	 * Gets the total fees declared in the block.
	 *
	 * Fees are computed by summing each transaction fees, including the coinbase
	 * one if it exists.
	 *
	 * @return a positive number
	 */
	public double getTotalFee() {
		double totalFee = this.transactions.getTotalFee();
		if (this.coinbaseTransaction != null) {
			totalFee += this.coinbaseTransaction.getFee();
		}
		return totalFee;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((blockNumber == null) ? 0 : blockNumber.hashCode());
		result = prime * result + ((nonce == null) ? 0 : nonce.hashCode());
		result = prime * result + ((previousBlockHashCode == null) ? 0 : previousBlockHashCode.hashCode());
		result = prime * result + ((timeStamp == null) ? 0 : timeStamp.hashCode());
		result = prime * result + ((listCoinbaseTransaction == null) ? 0 : listCoinbaseTransaction.hashCode());
		result = prime * result + ((transactions == null) ? 0 : transactions.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Block<Tx, T> other = (Block<Tx, T>) obj;
		if (blockNumber == null) {
			if (other.blockNumber != null)
				return false;
		} else if (!blockNumber.equals(other.blockNumber))
			return false;
		if (nonce == null) {
			if (other.nonce != null)
				return false;
		} else if (!nonce.equals(other.nonce))
			return false;
		if (previousBlockHashCode == null) {
			if (other.previousBlockHashCode != null)
				return false;
		} else if (!previousBlockHashCode.equals(other.previousBlockHashCode))
			return false;
		if (timeStamp == null) {
			if (other.timeStamp != null)
				return false;
		} else if (!timeStamp.equals(other.timeStamp))
			return false;
		if (transactions == null) {
			if (other.transactions != null)
				return false;
		} else if (!transactions.equals(other.transactions))
			return false;
		if (listCoinbaseTransaction == null) {
			if (other.listCoinbaseTransaction != null)
				return false;
		} else if (!listCoinbaseTransaction.equals(other.listCoinbaseTransaction))
			return false;
		return true;
	}

	@Override
	public String toString() {
		String result = "Block[blockNumber=" + blockNumber + ",\n";
		result += "\t previousBlockHashCode=" + previousBlockHashCode + ",\n";
		result += "\t timeStamp=" + timeStamp + ",\n";
		result += "\t transactions=" + transactions.toString().replaceAll("(?m)^", "\t") + ",\n";
		result += "\t [nonce=" + nonce + ", hashCode=" + hashCode() + "],\n";
		result += "\t total fee=" + getTotalFee() + ",\n";
		result += "]\n";
		return result;
	}

	/**
	 * Copy the current block.
	 *
	 * @return a copy
	 */
	public Block<Tx, T> copy() {
		Block<Tx, T> copy = new Block<>(this.blockNumber, this.previousBlockHashCode, this.timeStamp);
		copy.setNonce(this.nonce);
		copy.transactions = this.transactions.copy();
		if (this.coinbaseTransaction != null) {
			copy.coinbaseTransaction = (Tx) this.coinbaseTransaction.copy();
		}
		if (this.listCoinbaseTransaction != null) {
			copy.listCoinbaseTransaction.addAll(this.listCoinbaseTransaction);
		}
		return copy;
	}

	/**
	 * Gets the list of transactions the block contains.
	 *
	 * @return a possibly empty list, but never {@code null}
	 */
	public List<Tx> getTransactions() {
		return transactions.asList();
	}

	/**
	 * Gets the internal {@link MerkleTree} containing transactions.
	 *
	 * @return a reference to the internal merkle tree
	 */
	public MerkleTree<Tx> getMerkleTree() {
		return transactions;
	}

	/**
	 * Sets the internal merkle tree to the provided one, allowing to replace all
	 * transactions.
	 *
	 * @param transactions transactions to set
	 */
	public void setMerkleTree(MerkleTree<Tx> transactions) {
		this.transactions = transactions;
	}

	/**
	 * Sets the coinbase transaction to the given value and adds it to the coinbase
	 * list.
	 *
	 * The transaction must have a {@code null} sender and a not {@code null}
	 * receiver. Otherwise, the transaction is silently ignored.
	 *
	 * @param transaction transaction to add
	 */
	public void addCoinbaseTransaction(Tx transaction) {
		if (transaction.getSender() == null && transaction.getReceiver() != null) {
			this.coinbaseTransaction = transaction;
			this.listCoinbaseTransaction.add(transaction);
		}
	}

	/**
	 * Is this block the genesis one (block number is 0) ?
	 *
	 * @return {@code true} if the block number is 0, else {@code false}.
	 */
	public boolean isGenesis() {
		return this.blockNumber == 0;
	}

	/**
	 * Is this block empty (no transactions are stored) ?
	 *
	 * @return {@code true} if the block has no transactions, {@code false}
	 *         otherwise.
	 */
	public boolean isEmpty() {
		return this.getTransactions().isEmpty();
	}

}
