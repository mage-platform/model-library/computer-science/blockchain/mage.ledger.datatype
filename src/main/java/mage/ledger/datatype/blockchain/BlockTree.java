/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package mage.ledger.datatype.blockchain;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import mage.ledger.datatype.ExceptionMessages;
import mage.ledger.datatype.Transaction;

/**
 * Block trees are advanced ledger representations supporting side chains (or forks).
 *
 * @param <Tx> Type of transactions.
 * @param <T>  Timestamp type of blocks.
 * 
 * @author Önder Gürcan
 * @author Aimen Djari
 * @author Guillaume Rakotomalala
 * @author Adel Mansour
 */
public class BlockTree<Tx extends Transaction, T extends Number> implements Iterable<Block<Tx, T>>, Serializable {

	/**
	 * Generated serial version UID.
	 */
	private static final long serialVersionUID = -5474895341740752765L;

	/** Store all blocks by hash. */
	private HashMap<Integer, Block<Tx, T>> blockTree = new HashMap<>();

	/** The list of head blocks (one per chain). */
	private List<Block<Tx, T>> headBlocks;

	/** The main chain. */
	private Blockchain<Tx, T> mainChain;

	/** The list of all chains living in the block tree. */
	private List<Blockchain<Tx, T>> allChains = new Vector<>();

	/** The genesis block. */
	private Block<Tx, T> genesisBlock;

	/**
	 * Creates a new {@link BlockTree} instance, starting with the provided genesis block.
	 *
	 * The tree is initialized with a single chain, starting with the provided genesis block.
	 *
	 * @param genesisBlock the genesis block
	 * @throws IllegalArgumentException the genesis block is {@code null}, its genesis number is 0
	 * 		or its parent hash is not 0
	 */
	public BlockTree(Block<Tx, T> genesisBlock) {
		if (genesisBlock == null) {
			throw new IllegalArgumentException(ExceptionMessages.GENESIS_BLOCK_CANNOT_BE_NULL);
		} else if (genesisBlock.getBlockNumber() != 0) {
			throw new IllegalArgumentException(ExceptionMessages.GENESIS_BLOCK_BLOCK_NUMBER_SHOULD_BE_EQUAL_ZERO);
		} else if (genesisBlock.getPreviousBlockHashCode() != 0) {
			throw new IllegalArgumentException(ExceptionMessages.GENESIS_BLOCK_SHOULD_NOT_HAVE_A_PREVIOUS_BLOCK);
		} else {
			this.blockTree.put(genesisBlock.hashCode(), genesisBlock);
			this.headBlocks = new Vector<>();
			this.headBlocks.add(genesisBlock);
			this.mainChain = new Blockchain<>(genesisBlock);
			this.allChains.add(mainChain);
			this.genesisBlock = genesisBlock;
		}
	}

	/**
	 * Returns the block of the main chain corresponding to the number given in
	 * parameter or {@code null} if no block with this number exists.
	 *
	 * @param blockNumber number of the block
	 * @return the block instance or {@code null}
	 */
	public Block<Tx, T> getBlockByNumber(int blockNumber) {
		return mainChain.getBlockByNumber(blockNumber);
	}

	/**
	 * Returns the block which has the hash corresponding to the provided hash or {@code null} if no
	 * block with this hash is stored in the block tree.
	 *
	 * @param hashCode hash of the block
	 * @return the block instance or {@code null}
	 */
	public Block<Tx, T> getBlockByHashcode(int hashCode) {
		return blockTree.get(hashCode);
	}

  /**
   * Takes the hash of a block in parameter, searches for it in the blockchain and, then returns a
   * list of hashes of the blocks coming after the one which has the hash given in parameter.
	 *
	 * @param hashCode hash of the block
	 * @return a possibly empty list of hashes
   */
  public List<Integer> getMissingBlocks(int hashCode) {
		List<Integer> result = new Vector<>();
		int nbBlock = 0;

		for (Block<Tx, T> block : blockTree.values()) {
			if (block.hashCode() == hashCode) {
				nbBlock = block.getBlockNumber();
			}
		}

		for (Block<Tx, T> block : blockTree.values()) {
			if (block.getBlockNumber() > nbBlock) {
				result.add(block.hashCode());
			}
		}

		return result;
	}

	/**
	 * Returns a list of all the blockchains (main and side chains).
	 *
	 * @return a list of blockchains with at least one blockchain (the main chain)
	 */
	public List<Blockchain<Tx, T>> getAllChains() {
		return this.allChains;
	}

	/**
	 * Recalculates the list of all the chains (main and side chains).
	 */
	private void recalculateAllChains() {
		List<Vector<Block<Tx, T>>> blockchains = new Vector<>();
		boolean notYetGenesisBlock = true;
		while (allChains.size() < headBlocks.size()) {
			this.allChains.add(new Blockchain<Tx, T>(genesisBlock));
		}
		for (int i = 0; i < headBlocks.size(); i++) {
			List<Block<Tx, T>> result;
			// the first chain is already created, if not first, add one

			// add a new chain to the list chains
			blockchains.add(new Vector<>());
			// add headblock of the chain to its list
			blockchains.get(i).add(headBlocks.get(i));
			Block<Tx, T> headBlock = headBlocks.get(i);

			// while the block added is not the genesis block, continue to search and add
			// blocks
			while (notYetGenesisBlock) {
				for (Block<Tx, T> block : blockTree.values()) {
					if (block.hashCode() == headBlock.getPreviousBlockHashCode()) {
						blockchains.get(i).add(block);
						headBlock = block;
						// if block is genesis block, break the while loop
						if (headBlock.getBlockNumber() == 0) {
							notYetGenesisBlock = false;
						}
					}
				}
			}
			notYetGenesisBlock = true;

			// save the chain into a new list
			result = blockchains.get(i);

			// the chain being listed from headblock to genesis, it has to be reversed
			Collections.reverse(result);

			// put the new copy of this chain in "allChains", the global list of Chains
			this.allChains.get(i).clear();
			for (Block<Tx, T> block : result) {
				this.allChains.get(i).append(block);
			}

		}
	}

	/**
	 * Returns the main chain (the longest chain).
	 *
	 * @return the main chain, never {@code null}
	 */
	public Blockchain<Tx, T> getMainChain() {
		return this.mainChain;
	}

	/**
	 * Returns the internal block storage (hash to block).
	 *
	 * @return a reference to the internal block storage.
	 */
	public Map<Integer, ? extends Block<Tx, T>> getBlockChain() {
		return this.blockTree;
	}

	/**
	 * Recalculates the main chain (the longest chain).
	 */
	private void recalculateMainChain() {
		List<Block<Tx, T>> result = new Vector<>();
		int maxHeadBlockNumber = -1;
		Block<Tx, T> headBlock = headBlocks.get(0);
		boolean notYetGenesisBlock = true;

		// search for the maxHeadBlockNumber headblock
		for (Block<Tx, T> txTBlock : headBlocks) {
			if (maxHeadBlockNumber < txTBlock.getBlockNumber()) {
				headBlock = txTBlock;
				maxHeadBlockNumber = headBlock.getBlockNumber();
			}
		}

		// add headBlock found to the list
		result.add(headBlock);

		// while the block added is not genesis block, continue to search and add blocks
		while (notYetGenesisBlock) {
			for (Block<Tx, T> block : blockTree.values()) {
				// if new block matches with the one in the list, add it
				if (headBlock.getPreviousBlockHashCode() == block.hashCode()) {
					result.add(block);
					headBlock = block;
					// if the block added is the genesis one, break the while loop
					if (block.getBlockNumber() == 0) {
						notYetGenesisBlock = false;
					}
				}
			}
		}
		// The blockchain is built from the headblock to the genesis block

		// The list goes from headblock to genesis block, it needs to be reversed
		Collections.reverse(result);
		this.mainChain.clear();

		// Save the mainChain
		for (Block<Tx, T> block : result) {
			this.mainChain.append(block);
		}
	}

	/**
	 * Searches a transaction in the block tree using the provided hash.
	 *
	 * @param hashCode hash of the transaction
	 * @return the transaction or {@code null} if the transaction doesn't exist
	 */
	// TODO This method has not been unit tested.
	public Tx getTransactionByHashcode(int hashCode) {
		int size = blockTree.size();
		if (size > 0) {
			for (Block<Tx, T> block : blockTree.values()) {
				if (block.getTransaction(hashCode) != null) {
					return block.getTransaction(hashCode);
				}
			}
		}
		return null;
	}

	/**
	 * Gets the head block of the main chain. If there is no main chain (there are
	 * several side chains with the same height), it means that this blockchain is
	 * not stable, and thus it throws an UnsupportedOperationException.
	 *
	 * @return the head of the main chain
	 * @throws UnsupportedOperationException if there are several head block
	 */
	public Block<Tx, T> getHeadBlock() {
		// find the maximum chain height
		int maxChainHeight = 0;
		for (Block<Tx, T> headBlock : headBlocks) {
			if (maxChainHeight < headBlock.getBlockNumber()) {
				maxChainHeight = headBlock.getBlockNumber();
			}
		}

		// find the number of chains with the maximum height
		int numberOfChainsWithMaxHeight = 0;
		for (Block<Tx, T> headBlock : headBlocks) {
			if (maxChainHeight == headBlock.getBlockNumber()) {
				numberOfChainsWithMaxHeight++;
			}
		}

		// if there is more than one head block
		if (numberOfChainsWithMaxHeight > 1) {
			throw new UnsupportedOperationException(ExceptionMessages.BLOCKCHAIN_IS_NOT_STABLE);
		}

		return getMainChain().getBlockByNumber(maxChainHeight);
	}

	/**
	 * If the Blockchain is not stable, there is more than one headblock, so until
	 * one chain emerges, this method returns one of these headblocks.
	 *
	 * @return one of the head blocks
	 */
	public Block<Tx, T> getHeadBlockWithNotStableBlockchain() {
		// find the maximum chain height
		int maxChainHeight = 0;
		for (Block<Tx, T> headBlock : headBlocks) {
			if (maxChainHeight < headBlock.getBlockNumber()) {
				maxChainHeight = headBlock.getBlockNumber();
			}
		}

		return getMainChain().getBlockByNumber(maxChainHeight);
	}

	/**
	 * Gets the head blocks of each side chain.
	 *
	 * @return a list of blocks, with at least one block (main chain).
	 */
	public List<Block<Tx, T>> getAllHeadBlocks() {
		return headBlocks;
	}

	/**
	 * Appends a block to the blockchain if this block is referring to a valid
	 * parent block and a valid block number. If the new block has already been
	 * appended, it should not be added again, however a new block can be appended
	 * even if there is another block with the same block number (which allows
	 * forks).
	 * 
	 * @param block the new block to be appended
	 * @throws IllegalArgumentException if block is rejected
	 */
	public void append(Block<Tx, T> block) {
		boolean newFork = true;
		int headBlock = -1;
		// add the new block to the chains we can add it in
		for (Block<Tx, T> refBlock : blockTree.values()) {
			if (headBlock < refBlock.getBlockNumber()) {
				headBlock = refBlock.getBlockNumber();
			} else if (headBlock < block.getBlockNumber()) {
				headBlock = block.getBlockNumber();
			}

		}
		// if Block can be linked to the previous block, add it
		if (verifyBlock(block)) {
			if (this.blockTree.get(block.hashCode()) == null) {
				this.blockTree.put(block.hashCode(), block);
				// search for the previous block in the headblocks list and replace it
				for (int i = 0; i < headBlocks.size(); i++) {
					if (headBlocks.get(i).hashCode() == block.getPreviousBlockHashCode()) {
						this.headBlocks.set(i, block);
						newFork = false;
					}
				}

				// if no block found in the headblocks list, it's a fork
				if (newFork) {
					this.headBlocks.add(block);
				}

				recalculateAllChains();
				recalculateMainChain();
			} else {
				throw new IllegalArgumentException(
						String.format("Block number %d already exists in the blockchain and thus it is invalid and rejected.", block.getBlockNumber()));
			}
		} else {
			throw new IllegalArgumentException(
					String.format("Block number %d could not be verified and thus it is invalid and rejected.", block.getBlockNumber()));
		}
	}

	/**
	 * Checks if the given block is valid, meaning it has a parent and there is no block number gap.
	 *
	 * @param block block to check
	 * @return {@code true} if the block is valid, {@code false} otherwise
	 */
	public boolean verifyBlock(Block<Tx, T> block) {
		boolean validBlockNumber = false;
		boolean validPreviousHashCode = false;
		for (Block<Tx, T> refBlock : blockTree.values()) {
			if (refBlock.getBlockNumber() + 1 == block.getBlockNumber()) {
				validBlockNumber = true;
			}
			if (refBlock.hashCode() == block.getPreviousBlockHashCode()) {
				validPreviousHashCode = true;
			}
		}
		return (validBlockNumber & validPreviousHashCode);
	}

	/**
	 * Checks whether the provided transaction or block hash matches with a transaction or a block in
	 * the block tree.
	 *
	 * @param hashCode hash of the transaction or block
	 * @return {@code true} if a transaction or a block is found, {@code false} otherwise
	 */
	// TODO This method has not been unit tested.
	public boolean contains(Integer hashCode) {
		boolean hasBlock = getBlockByHashcode(hashCode) != null;
		boolean hasTransaction = getTransactionByHashcode(hashCode) != null;
		return hasBlock || hasTransaction;
	}

	/**
	 * Gets the total number of transactions in the block tree.
	 *
	 * @return a positive integer
	 */
	public Integer getNumberOfTransactions() {
		int result = 0;
		for (Block<Tx, T> block : blockTree.values()) {
			result += block.getTransactions().size();
		}

		return result;
	}

	/**
	 * Returns the total number of blocks in this blockTree (including all branches)
	 * 
	 * @return a positive non-nul integer
	 */
	public int getTotalNumberOfBlocks() {
		return blockTree.values().size();
	}

	@Override
	public String toString() {
		return "Blockchain [blockList=\n" + blockTree + "], numberOfTx=" + getNumberOfTransactions() + "\n";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((blockTree == null) ? 0 : blockTree.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BlockTree<Tx, T> other = (BlockTree<Tx, T>) obj;
		if (blockTree == null) {
			return other.blockTree == null;
		} else
			return blockTree.equals(other.blockTree);
	}

	/**
	 * Gets the size of the main chain.
	 *
	 * @return positive non-nul integer.
	 */
	public int size() {
		return this.mainChain.size();
	}

	/**
	 * Gets an {@link Iterator} instance iterating over blocks in the main chain.
	 *
	 * @return iterator over the main chain.
	 */
	@Override
	public Iterator<Block<Tx, T>> iterator() {
		return this.mainChain.iterator();
	}
}
