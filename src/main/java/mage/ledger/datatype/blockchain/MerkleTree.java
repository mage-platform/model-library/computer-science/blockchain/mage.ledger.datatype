/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package mage.ledger.datatype.blockchain;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Vector;

import mage.ledger.datatype.Transaction;

/**
 * An efficient structure to store transactions in blocks.
 *
 * Current implementation is not a real MerkleTree but will be changed in the future.
 *
 * @param <Tx> Type of transactions.
 * 
 * @author Önder Gürcan
 * @author Aimen Djari
 * @author Guillaume Rakotomalala
 */
public class MerkleTree<Tx extends Transaction> implements Serializable {

	/**
	 * Generated serial version UID. 
	 */
	private static final long serialVersionUID = -6345715468968760414L;
	
	/** Transaction storage. */
	private HashMap<Integer, Tx> hashedElements = new LinkedHashMap<>();

	/**
	 * Adds the given transaction to the tree.
	 *
	 * @param hashedElement the transaction
	 */
	public void addHashedElement(Tx hashedElement) {
		hashedElements.put(hashedElement.hashCode(), hashedElement);
	}

	/**
	 * Given the provided hash, retrieves the transaction.
	 *
	 * @param hashCode hash of the transaction
	 * @return Either the transaction or {@code null} if no transaction having the provided hash is
	 * 		stored.
	 */
	public Tx getHashedElement(int hashCode) {
		return hashedElements.get(hashCode);
	}

	/**
	 * Gets the total fees by summing up each transactions' fees.
	 *
	 * @return a positive number
	 */
	public double getTotalFee() {
		double totalFee = 0.0;
		Iterator<Tx> iterator = this.hashedElements.values().iterator();
		while (iterator.hasNext()) {
			Tx tx = iterator.next();
			totalFee += tx.getFee();
		}
		return totalFee;
	}

	@Override
	public String toString() {
		String result = "[\n";
		Iterator<Tx> iterator = hashedElements.values().iterator();
		while (iterator.hasNext()) {
			Tx transaction = iterator.next();
			result += "\t " + transaction.toString() + "\n";
		}
		result += " ]";
		return result;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((hashedElements.values().toString() == null) ? 0 : hashedElements.values().toString().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MerkleTree<Tx> other = (MerkleTree<Tx>) obj;
		if (hashedElements == null) {
			if (other.hashedElements != null)
				return false;
		} else if (!hashedElements.equals(other.hashedElements))
			return false;
		return true;
	}

	/**
	 * Returns a shallow copy of this merkle tree.
	 *
	 * @return a shallow copy
	 */
	public MerkleTree<Tx> copy() {
		MerkleTree<Tx> copy = new MerkleTree<>();
		copy.hashedElements = (HashMap<Integer, Tx>) this.hashedElements.clone();
		return copy;
	}

	/**
	 * Returns stored transactions as a list.
	 *
	 * @return a possibly empty list, but never {@code null}.
	 */
	public List<Tx> asList() {
		List<Tx> merkleTreeAsList = new Vector<>();
		Collection<Tx> values = hashedElements.values();
		merkleTreeAsList.addAll(values);
		return merkleTreeAsList;
	}

}
