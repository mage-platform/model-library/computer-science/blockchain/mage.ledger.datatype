/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package mage.ledger.datatype.blockchain;

import java.io.Serializable;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import mage.ledger.datatype.Transaction;

/**
 * Storage area for unconfirmed transactions.
 *
 * Transactions are indexed by their hash to allow quick retrieval.
 *
 * @param <Tx> Transaction's type
 * @author Önder Gürcan
 * @author Aimen Djari
 * @author Guillaume Rakotomalala
 */
public class MemoryPool<Tx extends Transaction> implements Serializable {

    /**
	 * Generated serial version UID. 
	 */
	private static final long serialVersionUID = -5873923704467456985L;
	
	/** Transactions storage. */
    private Hashtable<Integer, Tx> memoryPool;

    /**
     * Creates a new {@link MemoryPool} instance with no stored transactions.
     */
    public MemoryPool() {
        this.memoryPool = new Hashtable<Integer, Tx>();
    }

    /**
     * Puts the given transaction in the memory pool.
     *
     * @param hashCode hash of the transaction
     * @param transaction the transaction
     */
    public void put(Integer hashCode, Tx transaction) {
        this.memoryPool.put(hashCode, transaction);
    }

    /**
     * Gets an {@link Enumeration} of the transactions hashes.
     *
     * @return enumeration of hashes
     */
    public Enumeration<Integer> keys() {
        return this.memoryPool.keys();
    }

    /**
     * Same as {@link #keys()} but returns a list of hashes instead of an enumeration.
     *
     * @return a possibly empty list of hashes, but never {@code null}
     */
    public List<Integer> getKeysAsList() {
        List<Integer> keys = new Vector<Integer>();
        Enumeration<Integer> poolKeys = this.memoryPool.keys();
        while (poolKeys.hasMoreElements()) {
            Integer nextElement = poolKeys.nextElement();
            keys.add(nextElement);
        }
        return keys;
    }

    /**
     * Gets all the stored transaction.
     *
     * @return a possibly empty collection of transactions but never {@code null}.
     */
    public Collection<Tx> values() {
        return this.memoryPool.values();
    }

    /**
     * Checks if the given transaction's hash matches with a stored transaction.
     *
     * @param hashCode hash of the transaction
     * @return {@code true} if a transaction has the given hash, {@code false} otherwise
     */
    public boolean containsHashcode(Integer hashCode) {
        return this.memoryPool.containsKey(hashCode);
    }

    /**
     * Gets the transaction given the provided hash.
     *
     * @param hashCode hash of the transaction
     * @return either the transaction instance or {@code null} if there is no transaction with the
     *      provided hash.
     */
    public Tx get(Integer hashCode) {
        return this.memoryPool.get(hashCode);
    }

    /**
     * Removes a transaction using its hash.
     *
     * If the hash doesn't match with any stored transaction, this is a no-op.
     *
     * @param hashCode hash of the transaction to remove
     */
    public void remove(Integer hashCode) {
        this.memoryPool.remove(hashCode);
    }

    /**
     * Gets this pool's size.
     *
     * @return positive integer
     */
    public int size() {
        return this.memoryPool.values().size();
    }
    
    /**
     * Clears this memory pool so that it contains no transactions.
     */
    public void clear() {
    	this.memoryPool.clear();
    }

    @Override
    public String toString() {
        String result = "[\n";
        Iterator<Tx> iterator = memoryPool.values().iterator();
        while (iterator.hasNext()) {
            Tx transaction = iterator.next();
            result += "\t " + transaction.toString() + "\n";
        }
        result += " ]";
        return result;
    }

}
