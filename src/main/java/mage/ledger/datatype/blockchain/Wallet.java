/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package mage.ledger.datatype.blockchain;

import java.io.Serializable;
import java.util.Hashtable;

import mage.ledger.datatype.Address;

/**
 * A wallet stores the addresses which can be used to receive or spend a
 * cryptocurrency designated by the parameter key.
 * 
 * @author Önder Gürcan
 * @author Mohamed Aimen Djari
 *
 */
public class Wallet implements Serializable {

	/**
	 * Generated serial version UID. 
	 */
	private static final long serialVersionUID = 1291014256851473749L;
	
	/** A mapping crypo-name to address. */
	private Hashtable<String, Address> wallet;

	/**
	 * Creates a new empty {@link Wallet} instance.
	 */
	public Wallet() {
		this.wallet = new Hashtable<String, Address>();
	}

	/**
	 * Creates a new entry in the wallet.
	 *
	 * If some address was already associated to the provided key, it is replaced by the new one.
	 *
	 * @param key crypto name
	 * @param address wallet address
	 */
	public void addAddress(String key, Address address) {
		this.wallet.put(key, address);
	}

	/**
	 * Removes the address associated to the provided key.
	 *
	 * No-op if the key doesn't exist.
	 *
	 * @param key crypto name
	 */
	public void removeAddress(String key) {
		this.wallet.remove(key);
	}

	/**
	 * Gets the address associated to provided key.
	 *
	 * @param key crypto name
	 * @return Either the address instance or {@code null} if the key doesn't exist.
	 */
	public Address getAddress(String key) {
		return this.wallet.get(key);
	}

	/**
	 * Gets the wallet's size.
	 *
	 * @return positive integer
	 */
	public int size() {
		return this.wallet.size();
	}

}
