/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package mage.ledger.datatype;

import java.io.Serializable;

/**
 * An address can be any kind of physical or logical identifier that has a
 * unique string representation.
 *
 * @author Önder Gürcan
 */
public abstract class Address implements Serializable {

	/**
	 * Generated serial version UID.
	 */
	private static final long serialVersionUID = -2447683160296047126L;

	@Override
	public abstract String toString();

	/**
	 * If the string representations of two addresses are equal, these two addresses
	 * are considered to be the same.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		String other = obj.toString();
		return toString().equals(other);
	}
	
	

}
