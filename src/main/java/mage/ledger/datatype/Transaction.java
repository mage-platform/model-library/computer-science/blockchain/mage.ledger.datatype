/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package mage.ledger.datatype;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * A transaction is a transfer of assets value that is broadcast to the network
 * and collected into blocks. A transaction typically references previous
 * transaction outputs as new transaction inputs and dedicates all input assets
 * values to new outputs. Transactions are not encrypted, so it is possible to
 * browse and view every transaction ever collected into a block. Once
 * transactions are buried under enough confirmations they can be considered
 * irreversible.
 *
 * @author Önder Gürcan
 * @author Aimen Djari
 * @author Guillaume Rakotomalala
 * @author Nicolas Lagaillardie
 * @version 0.0.2
 */
public class Transaction implements Serializable {

    /**
	 * Generated serial version UID.
	 */
	private static final long serialVersionUID = 895532484739939391L;

	/**
     * The address of the sender of the assets
     */
    private Address sender;

    /**
     * The address of the receiver of the assets
     */
    private Address receiver;

    /**
     * The fee given by the sender to pay the block proposers
     */
    private Double fee;

    /**
     * The amount of assets transfered to the receiver from the sender
     */
    private Payload payload;

    /**
     * The size of the transaction
     */
    private Integer size;

    /**
     * Block height or time-stamp when transaction is final
     */
    private BigDecimal lockTime;
    
    /**
     * True if this transaction is endorsed. False by default.
     */
    private boolean isEndorsed;

    /**
     * Creates a new {@link Transaction} instance.
     *
     * @param senderAddress address of the transaction's sender
     * @param receiverAddress address of the transaction's receiver
     * @param fee fees to be paid
     * @param payload content of the transaction
     * @throws IllegalArgumentException fee must be greater or equal to 0, receiver can't be
     *      {@code null}
     */
    public Transaction(Address senderAddress, Address receiverAddress, Double fee, Payload payload) {
        if (fee < 0) {
            throw new IllegalArgumentException(ExceptionMessages.FEE_SHOULD_BE_HIGHER_THAN_ZERO);
        } else if (receiverAddress == null) {
            throw new IllegalArgumentException(ExceptionMessages.RECEIVER_ADDRESS_CANNOT_BE_NULL);
        } else {
            this.sender = senderAddress;
            this.receiver = receiverAddress;
            this.fee = fee;
            this.payload = payload;

            this.size = 256;
            
            this.isEndorsed = false;
        }
    }

  /**
   * Creates a new {@link Transaction} instance with a lock time.
   *
   * @param senderAddress address of the transaction's sender
   * @param receiverAddress address of the transaction's receiver
   * @param fee fees to be paid
   * @param payload content of the transaction
   * @param lockTime lock time
   * @throws IllegalArgumentException fee must be greater or equal to 0, receiver can't be
   *        {@code null} or lockTIme is less than 0
   */
  public Transaction(
      Address senderAddress,
      Address receiverAddress,
      Double fee,
      Payload payload,
      BigDecimal lockTime) {
        this(senderAddress, receiverAddress, fee, payload);

        if (lockTime != null && lockTime.compareTo(BigDecimal.ZERO) == -1) {
            throw new IllegalArgumentException(ExceptionMessages.LOCK_TIME_MUST_BE_POSITIVE);
        }

        this.lockTime = lockTime;
    }

    /**
     * Gets the transaction's sender.
     *
     * @return the sender, possibly {@code null}
     */
    public Address getSender() {
        return sender;
    }

    /**
     * Gets the transaction's receiver.
     *
     * @return the receiver, never {@code null}
     */
    public Address getReceiver() {
        return receiver;
    }

    /**
     * Gets transaction fees.
     *
     * @return a positive number
     */
    public Double getFee() {
        return fee;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((fee == null) ? 0 : fee.hashCode());
        result = prime * result + ((payload == null) ? 0 : payload.hashCode());
        result = prime * result + ((receiver == null) ? 0 : receiver.hashCode());
        result = prime * result + ((sender == null) ? 0 : sender.hashCode());
        result = prime * result + ((size == null) ? 0 : size.hashCode());
        result = prime * result + ((lockTime == null) ? 0 : lockTime.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Transaction other = (Transaction) obj;
        if (fee == null) {
            if (other.fee != null)
                return false;
        } else if (!fee.equals(other.fee))
            return false;
        if (payload == null) {
            if (other.payload != null)
                return false;
        } else if (!payload.equals(other.payload))
            return false;
        if (receiver == null) {
            if (other.receiver != null)
                return false;
        } else if (!receiver.equals(other.receiver))
            return false;
        if (sender == null) {
            if (other.sender != null)
                return false;
        } else if (!sender.equals(other.sender))
            return false;
        if (size == null) {
            if (other.size != null)
                return false;
        } else if (!size.equals(other.size))
            return false;
        if (lockTime == null) {
            if (other.lockTime != null)
                return false;
        } else if (!lockTime.equals(other.lockTime))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Transaction [sender=" + sender + ", receiver=" + receiver + ", fee=" + fee + ", payload=" + payload
                + ", lock time=" + lockTime + ", hashCode()=" + hashCode() + "]";
    }

    /**
     * Copies the current transaction.
     *
     * @return a copy of the transaction
     */
    public Transaction copy() {
        Transaction copy = new Transaction(sender, receiver, fee, payload, lockTime);
        copy.size = this.size;
        return copy;
    }

    /**
     * Gets the transaction's payload.
     *
     * @return the payload, possibly {@code null}
     */
    public Payload getPayload() {
        return this.payload;
    }

    /**
     * Get the transaction's lock time.
     *
     * @return a positive number or {@code null}
     */
    public BigDecimal getLockTime() {
        return this.lockTime;
    }

    /**
     * Tells if the transaction is endorsed or not.
     *
     * @return endorsement status
     */
	public boolean isEndorsed() {
		return isEndorsed;
	}

    /**
     * Sets the endorsement status to the provided value.
     *
     * @param isEndorsed new value
     */
	public void setEndorsed(boolean isEndorsed) {
		this.isEndorsed = isEndorsed;
	}

    /**
     * Checks if the provided address is the receiving address of the transaction.
     * Future implementations could support multiple receiving addresses.
     *
     * @param address address to check
     * @return {@code true} if the provided address is the receiving address, {@code false}
     *      otherwise.
     */
	public boolean hasReceivingAddress(final Address address) {
        return address != null && address.equals(receiver);
    }
}
