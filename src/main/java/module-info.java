module mage.blockchain.datatype {
    exports mage.ledger.datatype.blockchain;
    exports mage.ledger.datatype.lightning;
    exports mage.ledger.datatype;
}