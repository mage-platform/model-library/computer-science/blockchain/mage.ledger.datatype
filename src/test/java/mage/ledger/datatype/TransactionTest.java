/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package mage.ledger.datatype;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNotSame;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.math.BigDecimal;

import org.junit.jupiter.api.Test;

import mage.ledger.datatype.Address;
import mage.ledger.datatype.ExceptionMessages;
import mage.ledger.datatype.Payload;
import mage.ledger.datatype.Transaction;
import mage.ledger.datatype.UUIDAddress;

/**
 * A transaction is issued from a sender address to a receiver address with a
 * given fee. The sender and the receiver addresses may be the same. The sender
 * address can be null where as the receiver address cannot be null. A
 * transaction has an integer fee value higher than or equal to zero. (see
 * {@link #createValid()} and {@link #createInvalid()}) Each transaction has a
 * unique hashcode value. (see {@link #compareHashcodes()}) Transactions can be
 * exactly copied since each node stores a copy of them.
 *
 * @author Önder Gürcan
 * @version 0.0.2
 */
public class TransactionTest {

    Address senderAddress = new UUIDAddress();
    Address receiverAddress = new UUIDAddress();

    @Test
    public void payloadIsNumerical() {
        Payload payload = new Payload(5);
        assertTrue(payload.isNumerical());
        assertEquals(5.d, payload.getNumericalValue(), 0.00001);
    }

    @Test
    public void payloadNotNumerical() {
        Payload payload = new Payload("TEST");
        assertFalse(payload.isNumerical());
        assertEquals(0.d, payload.getNumericalValue(), 0.00001d);
    }

    @Test
    public void createValid() {
        Double positiveFee = Double.valueOf(5);
        Double zeroFee = Double.valueOf(0);
        Double zeroPayload = Double.valueOf(0);

        try {
            Transaction transaction = new Transaction(senderAddress, receiverAddress, positiveFee,
                    new Payload(zeroPayload));
            assertNotNull(transaction);
            assertEquals(transaction.getFee(), positiveFee);
        } catch (Exception e) {
            fail(ExceptionMessages.THIS_LINE_SHOULD_NOT_BE_EXECUTED);
        }

        try {
            Transaction transaction = new Transaction(senderAddress, receiverAddress, zeroFee,
                    new Payload(zeroPayload));
            assertNotNull(transaction);
            assertEquals(transaction.getFee(), zeroFee);
        } catch (Exception e) {
            fail(ExceptionMessages.THIS_LINE_SHOULD_NOT_BE_EXECUTED);
        }

        try {
            Transaction transaction = new Transaction(senderAddress, receiverAddress, zeroFee,
                    new Payload(zeroPayload), BigDecimal.ONE);
            assertNotNull(transaction);
            assertEquals(transaction.getFee(), zeroFee);
        } catch (Exception e) {
            fail(ExceptionMessages.THIS_LINE_SHOULD_NOT_BE_EXECUTED);
        }
    }

    @Test
    public void createInvalid() {
        try {
            new Transaction(senderAddress, receiverAddress, (double) -1, new Payload(0));
            fail(ExceptionMessages.THIS_LINE_SHOULD_NOT_BE_EXECUTED);
        } catch (Exception e) {
            assertTrue(e instanceof IllegalArgumentException);
            assertEquals(e.getMessage(), ExceptionMessages.FEE_SHOULD_BE_HIGHER_THAN_ZERO);
        }

        try {
            new Transaction(senderAddress, receiverAddress, (double) -5, new Payload(0));
            fail(ExceptionMessages.THIS_LINE_SHOULD_NOT_BE_EXECUTED);
        } catch (Exception e) {
            assertTrue(e instanceof IllegalArgumentException);
            assertEquals(e.getMessage(), ExceptionMessages.FEE_SHOULD_BE_HIGHER_THAN_ZERO);
        }

        try {
            new Transaction(senderAddress, null, (double) 5, new Payload(0));
            fail(ExceptionMessages.THIS_LINE_SHOULD_NOT_BE_EXECUTED);
        } catch (Exception e) {
            assertTrue(e instanceof IllegalArgumentException);
            assertEquals(e.getMessage(), ExceptionMessages.RECEIVER_ADDRESS_CANNOT_BE_NULL);
        }

        try {
            new Transaction(senderAddress, receiverAddress, (double) 5, new Payload(0), BigDecimal.valueOf(-1));
            fail(ExceptionMessages.THIS_LINE_SHOULD_NOT_BE_EXECUTED);
        } catch (Exception e) {
            assertTrue(e instanceof IllegalArgumentException);
            assertEquals(e.getMessage(), ExceptionMessages.LOCK_TIME_MUST_BE_POSITIVE);
        }
    }

    @Test
    public void compareHashcodes() {
        Double fee1 = 5.0;
        Double fee2 = 7.0;

        Transaction transaction1 = new Transaction(senderAddress, receiverAddress, fee1, new Payload(0));
        Transaction transaction2 = new Transaction(senderAddress, receiverAddress, fee2, new Payload(0));

        assertNotEquals(transaction1.hashCode(), transaction2.hashCode());
    }

    @Test
    public void copy() {
        Double fee = 5.0;
        Integer payload = 0;
        BigDecimal lockTime = BigDecimal.ONE;

        // create a transaction using the 1st constructor
        Transaction transaction1 = new Transaction(senderAddress, receiverAddress, fee, new Payload(payload));
        Transaction copy1 = transaction1.copy();

        assertNotSame(transaction1, copy1);
        assertEquals(transaction1, copy1);

        // create a transaction using the 2st constructor (with lock_time)
        Transaction transaction2 = new Transaction(senderAddress, receiverAddress, fee, new Payload(payload),
                lockTime);
        Transaction copy2 = transaction2.copy();

        assertNotSame(transaction2, copy2);
        assertEquals(transaction2, copy2);
    }

}
