/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package mage.ledger.datatype.blockchain;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.List;
import java.util.Vector;

import org.junit.jupiter.api.Test;

import mage.ledger.datatype.Address;
import mage.ledger.datatype.ExceptionMessages;
import mage.ledger.datatype.Payload;
import mage.ledger.datatype.Transaction;
import mage.ledger.datatype.UUIDAddress;

/**
 * Blockchain is an append-only linked list of blocks.
 * <p>
 * A blockchain must have a genesis block whose block number is zero. (see
 * {@link #createValid()}). The head block is the last block of the main chain.
 * A blocks can only be appended by referencing the head block. A new block
 * becomes the head block.
 *
 * @author Önder Gürcan
 * @author Mohamed Aimen Djari
 */
public class BlockTreeTest {

	@Test
	public void createValid() {
		Block<Transaction, ?> genesisBlock = new Block<>(0, 0, 0L);
		// TODO Block<P,T> should be parameterized
		//
		BlockTree<Transaction, ?> blockchain = new BlockTree(genesisBlock);
		assertNotNull(blockchain);
		assertNotNull(blockchain.getBlockByNumber(0));
		assertEquals(Integer.valueOf(0), blockchain.getBlockByNumber(0).getBlockNumber());
		assertEquals(0, blockchain.getBlockByNumber(0).getPreviousBlockHashCode());
		assertEquals(0L, genesisBlock.getTimeStamp());

		assertEquals(genesisBlock, blockchain.getBlockByNumber(0));
		//
		assertEquals(genesisBlock, blockchain.getHeadBlock());
	}

	@Test
	public void createInvalid() {
		try {
			new BlockTree(null);
			fail(ExceptionMessages.THIS_LINE_SHOULD_NOT_BE_EXECUTED);
		} catch (IllegalArgumentException e) {
			assertEquals(ExceptionMessages.GENESIS_BLOCK_CANNOT_BE_NULL, e.getMessage());
		}

		try {
			new BlockTree(new Block(1, 0, 0L));
			fail(ExceptionMessages.THIS_LINE_SHOULD_NOT_BE_EXECUTED);
		} catch (IllegalArgumentException e) {
			assertEquals(ExceptionMessages.GENESIS_BLOCK_BLOCK_NUMBER_SHOULD_BE_EQUAL_ZERO, e.getMessage());
		}

		try {
			new BlockTree(new Block(0, 1, 0L));
			fail(ExceptionMessages.THIS_LINE_SHOULD_NOT_BE_EXECUTED);
		} catch (IllegalArgumentException e) {
			assertEquals(ExceptionMessages.GENESIS_BLOCK_SHOULD_NOT_HAVE_A_PREVIOUS_BLOCK, e.getMessage());
		}
	}

	@Test
	public void appendValid() {
		BlockTree blockchain = new BlockTree(new Block(0, 0, 0L));
		Block headBlock = blockchain.getHeadBlock();
		//
		Block block = new Block(1, headBlock.hashCode(), 0L);
		blockchain.append(block);
		assertNotEquals(headBlock, blockchain.getHeadBlock());
		assertEquals(block, blockchain.getHeadBlock());
	}

	@Test
	public void appendInvalid() {
		BlockTree blockchain = new BlockTree(new Block(0, 0, 0L));
		Block headBlock = blockchain.getHeadBlock();
		//
		// new block does not follow the block number of the head block
		Block block = new Block(2, headBlock.hashCode(), 0L);
		try {
			blockchain.append(block);
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals(headBlock, blockchain.getHeadBlock());
			assertNotEquals(block, blockchain.getHeadBlock());
		}
		
		//
		// new block does not reference the head block
		block = new Block(1, 0, 0L);
		try {
			blockchain.append(block);
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals(headBlock, blockchain.getHeadBlock());
			assertNotEquals(block, blockchain.getHeadBlock());
		}
		
	}

	/**
	 * Verify if the size methods works correctly when there is no fork.
	 */
	@Test
	public void testSizeNoFork() {
		BlockTree blockchain = new BlockTree(new Block(0, 0, 0L));
		Block headBlock = blockchain.getHeadBlock();
		assert blockchain.size() == 1;

		Block block = new Block(1, headBlock.hashCode(), 0L);
		blockchain.append(block);
		assert blockchain.size() == 2;
	}

	/**
	 * Test if the size methods works when there is fork.
	 */
	@Test
	public void testSizeWithFork() {
		BlockTree blockchain = new BlockTree(new Block(0, 0, 0L));
		Block headBlock = blockchain.getHeadBlock();
		assert blockchain.size() == 1;

		Block b1 = new Block(1, headBlock.hashCode(), 0L);
		blockchain.append(b1);
		assert blockchain.size() == 2;

		Block b2 = new Block(2, b1.hashCode(), 0L);
		blockchain.append(b2);
		assert blockchain.size() == 3;

		Block b3 = new Block(2, b1.hashCode(), 2L);
		blockchain.append(b3);
		assert blockchain.size() == 3;

		Block b4 = new Block(3, b2.hashCode(), 0L);
		blockchain.append(b4);
		assert blockchain.size() == 4;

		Block b5 = new Block(3, b3.hashCode(), 0L);
		blockchain.append(b5);
		assert blockchain.size() == 4;

		Block b6 = new Block(4, b5.hashCode(), 0L);
		blockchain.append(b6);
		assert blockchain.size() == 5;
	}

	/**
	 * Tests if the method iterator works with no fork.
	 */
	@Test
	public void iteratorNoFork() {
		List<Block<Transaction, Long>> listBlock = new Vector<>();

		BlockTree<Transaction, Long> blockchain = new BlockTree<>(new Block<>(0, 0, 0L));
		Block<Transaction, Long> headBlock = blockchain.getHeadBlock();
		listBlock.add(headBlock);

		Block<Transaction, Long> block = new Block<>(1, headBlock.hashCode(), 0L);
		blockchain.append(block);
		listBlock.add(block);

		int i = 0;
		for (Block<Transaction, Long> b : blockchain) {
			assertEquals(b, listBlock.get(i));
			i++;
		}
	}

	/**
	 * Tests if the method iterator works with fork.
	 */
	@Test
	public void iteratorWithFork() {
		List<Block<Transaction, Long>> lb1 = new Vector<>();
		List<Block<Transaction, Long>> lb2 = new Vector<>();

		int i = 0;

		BlockTree<Transaction, Long> blockchain = new BlockTree<>(new Block<>(0, 0, 0L));
		Block<Transaction, Long> headBlock = blockchain.getHeadBlock();
		lb1.add(headBlock);
		lb2.add(headBlock);

		i = 0;
		for (Block<Transaction, Long> b : blockchain) {
			assertEquals(b, lb1.get(i));
			i++;
		}

		Block<Transaction, Long> b1 = new Block<>(1, headBlock.hashCode(), 0L);
		blockchain.append(b1);
		lb1.add(b1);
		lb2.add(b1);

		i = 0;
		for (Block<Transaction, Long> b : blockchain) {
			assertEquals(b, lb1.get(i));
			i++;
		}

		Block<Transaction, Long> b2 = new Block<>(2, b1.hashCode(), 0L);
		blockchain.append(b2);
		lb1.add(b2);

		i = 0;
		for (Block<Transaction, Long> b : blockchain) {
			assertEquals(b, lb1.get(i));
			i++;
		}

		Block<Transaction, Long> b3 = new Block<>(2, b1.hashCode(), 2L);
		blockchain.append(b3);
		lb2.add(b3);

		i = 0;
		for (Block<Transaction, Long> b : blockchain) {
			assertEquals(b, lb1.get(i));
			i++;
		}

		Block<Transaction, Long> b4 = new Block<>(3, b2.hashCode(), 0L);
		blockchain.append(b4);
		lb1.add(b4);

		i = 0;
		for (Block<Transaction, Long> b : blockchain) {
			assertEquals(b, lb1.get(i));
			i++;
		}

		Block<Transaction, Long> b5 = new Block<>(3, b3.hashCode(), 0L);
		blockchain.append(b5);
		lb2.add(b5);

		i = 0;
		for (Block<Transaction, Long> b : blockchain) {
			assertEquals(b, lb1.get(i));
			i++;
		}

		Block<Transaction, Long> b6 = new Block<>(4, b5.hashCode(), 0L);
		blockchain.append(b6);
		lb2.add(b6);

		i = 0;
		for (Block<Transaction, Long> b : blockchain) {
			assertEquals(b, lb2.get(i));
			i++;
		}
	}

	/**
	 * Creates a blockchain with 2 side chains where they have the same height.
	 * Consequently, there is no main chain and the blockchain does not have a head
	 * block.
	 */
	@Test
	public void forkValid() {
		BlockTree blockchain = new BlockTree(new Block(0, 0, 0L));
		Block headBlock = blockchain.getHeadBlock();

		// create a block with block number 1
		Block block1a = new Block(1, headBlock.hashCode(), 0L);
		Address senderAddress = new UUIDAddress();
		Address receiverAddress = new UUIDAddress();
		List<Transaction> transactionsList = new Vector<Transaction>();
		transactionsList.add(new Transaction(senderAddress, receiverAddress, (double) 5, new Payload(0)));
		block1a.addTransactions(transactionsList);
		blockchain.append(block1a);
		
		// create another block with block number 1 again
		Block block1b = new Block(1, headBlock.hashCode(), 0L);
		transactionsList.add(new Transaction(senderAddress, receiverAddress, (double) 10, new Payload(0)));
		block1b.addTransactions(transactionsList);
		blockchain.append(block1b);

		// there should be two side chains with the same height
		List<Blockchain> allChains = blockchain.getAllChains();
		// verify that there are two side chains
		assertEquals(2, allChains.size());
		// verify that the height of the side chains are equal
		Blockchain chain1 = allChains.get(0);
		Blockchain chain2 = allChains.get(1);
		assertEquals(chain1.size(), chain2.size());
		// verify that the head blocks for the side chains are as expected
		assertEquals(block1a, chain1.getHeadBlock());
		assertEquals(block1b, chain2.getHeadBlock());

		// since the side chains have the same height, there should not be a main chain
		try {
			// thus it should not be possible to retrieve the head block
			assertEquals(block1a, blockchain.getHeadBlock());
			assertEquals(block1b, blockchain.getHeadBlock());
			fail(ExceptionMessages.THIS_LINE_SHOULD_NOT_BE_EXECUTED);
		} catch (Exception e) {
			assertTrue(e instanceof UnsupportedOperationException);
			assertTrue(e.getMessage().equals(ExceptionMessages.BLOCKCHAIN_IS_NOT_STABLE));
		}

	}

	/**
	 * Creates a blockchain with 2 side chains where one of the chain is longer than
	 * the other one. Consequently, the longer chain is the main chain and the
	 * blockchain has a head block.
	 */
	@Test
	public void getMainChain() {
		BlockTree blockchain = new BlockTree(new Block(0, 0, 0L));
		Block genesisBlock = null;
		try {
			genesisBlock = blockchain.getHeadBlock();
		} catch (Exception e) {
			genesisBlock = blockchain.getHeadBlockWithNotStableBlockchain();
		}
		// create a block with block number 1
		Block block1a = new Block(1, genesisBlock.hashCode(), 0L);
		Address senderAddress = new UUIDAddress();
		Address receiverAddress = new UUIDAddress();
		List<Transaction> transactionsList = new Vector<Transaction>();
		transactionsList.add(new Transaction(senderAddress, receiverAddress, (double) 5, new Payload(0)));
		block1a.addTransactions(transactionsList);
		blockchain.append(block1a);

		// create another block with block number 1 again
		Block block1b = new Block(1, genesisBlock.hashCode(), 0L);
		block1b.setNonce(10);
		transactionsList.add(new Transaction(senderAddress, receiverAddress, (double) 10, new Payload(0)));
		block1b.addTransactions(transactionsList);
		blockchain.append(block1b);

		// create another block with block number 2 connected to block1b
		Block block2b = new Block(2, block1b.hashCode(), 0L);
		block2b.setNonce(10);
		transactionsList.add(new Transaction(senderAddress, receiverAddress, (double) 15, new Payload(0)));
		block2b.addTransactions(transactionsList);
		blockchain.append(block2b);

		// since the side chains does not have the same height, there should be a main
		// chain
		try {
			// thus it should be possible to retrieve the head block
			assertEquals(block2b, blockchain.getHeadBlock());
		} catch (Exception e) {
			fail(ExceptionMessages.THIS_LINE_SHOULD_NOT_BE_EXECUTED);
		}

		// verify all the blocks in the main chain
		Blockchain mainChain = blockchain.getMainChain();
		assertEquals(genesisBlock, mainChain.getBlockByNumber(0));
		assertEquals(block1b, mainChain.getBlockByNumber(1));
		assertEquals(block2b, mainChain.getBlockByNumber(2));
		assertFalse(mainChain.contains(block1a));

	}

	@Test
	public void getTotalNumberOfBlocks() {
		BlockTree blockchain = new BlockTree(new Block(0, 0, 0L));

		assertEquals(1, blockchain.getTotalNumberOfBlocks());

		Block genesisBlock = blockchain.getHeadBlock();

		// create a block with block number 1
		Block block1a = new Block(1, genesisBlock.hashCode(), 0L);
		Address senderAddress = new UUIDAddress();
		Address receiverAddress = new UUIDAddress();
		List<Transaction> transactionsList = new Vector<Transaction>();
		transactionsList.add(new Transaction(senderAddress, receiverAddress, (double) 5, new Payload(0)));
		block1a.addTransactions(transactionsList);
		blockchain.append(block1a);

		assertEquals(2, blockchain.getTotalNumberOfBlocks());

		// create another block with block number 1 again
		Block block1b = new Block(1, genesisBlock.hashCode(), 0L);
		block1b.setNonce(10);
		transactionsList.add(new Transaction(senderAddress, receiverAddress, (double) 10, new Payload(0)));
		block1b.addTransactions(transactionsList);
		blockchain.append(block1b);

		assertEquals(3, blockchain.getTotalNumberOfBlocks());

		// create another block with block number 2 connected to block1b
		Block block2b = new Block(2, block1b.hashCode(), 0L);
		block2b.setNonce(10);
		transactionsList.add(new Transaction(senderAddress, receiverAddress, (double) 15, new Payload(0)));
		block2b.addTransactions(transactionsList);
		blockchain.append(block2b);

		assertEquals(4, blockchain.getTotalNumberOfBlocks());

	}

}
