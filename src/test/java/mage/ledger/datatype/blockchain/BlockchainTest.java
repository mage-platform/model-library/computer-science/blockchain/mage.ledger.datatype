/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package mage.ledger.datatype.blockchain;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.math.BigDecimal;
import java.util.List;
import java.util.Vector;

import org.junit.jupiter.api.Test;

import mage.ledger.datatype.Address;
import mage.ledger.datatype.ExceptionMessages;
import mage.ledger.datatype.Payload;
import mage.ledger.datatype.Transaction;
import mage.ledger.datatype.UUIDAddress;

/**
 * Blockchain is an append-only linked list of blocks.
 * <p>
 * A blockchain must have a genesis block whose block number is zero. (see
 * {@link #createValid()}). The head block is the last block of the main chain.
 * A blocks can only be appended by referencing the head block. A new block
 * becomes the head block.
 *
 * @author Önder Gürcan
 * @author Mohamed Aimen Djari
 */
public class BlockchainTest {

	@Test
	public void createValid() {
		Block<Transaction, ?> genesisBlock = new Block<>(0, 0, 0L);
		// TODO Block<P,T> should be parameterized
		//
		Blockchain<Transaction, ?> chain = new Blockchain<>(genesisBlock);
		assertNotNull(chain);
		assertNotNull(chain.getBlockByNumber(0));
		assertEquals(Integer.valueOf(0), chain.getBlockByNumber(0).getBlockNumber());
		assertEquals(0, chain.getBlockByNumber(0).getPreviousBlockHashCode());
		assertEquals(0L, genesisBlock.getTimeStamp());

		assertEquals(genesisBlock, chain.getBlockByNumber(0));
		//
		assertEquals(genesisBlock, chain.getHeadBlock());
	}

	@Test
	public void createInvalid() {
		try {
			new Blockchain<>(null);
			fail(ExceptionMessages.THIS_LINE_SHOULD_NOT_BE_EXECUTED);
		} catch (IllegalArgumentException e) {
			assertEquals(ExceptionMessages.GENESIS_BLOCK_CANNOT_BE_NULL, e.getMessage());
		}

		try {
			new Blockchain<>(new Block<>(1, 0, 0L));
			fail(ExceptionMessages.THIS_LINE_SHOULD_NOT_BE_EXECUTED);
		} catch (IllegalArgumentException e) {
			assertEquals(ExceptionMessages.GENESIS_BLOCK_BLOCK_NUMBER_SHOULD_BE_EQUAL_ZERO, e.getMessage());
		}

		try {
			new Blockchain<>(new Block<>(0, 1, 0L));
			fail(ExceptionMessages.THIS_LINE_SHOULD_NOT_BE_EXECUTED);
		} catch (IllegalArgumentException e) {
			assertEquals(ExceptionMessages.GENESIS_BLOCK_SHOULD_NOT_HAVE_A_PREVIOUS_BLOCK, e.getMessage());
		}
	}

	/**
	 * Tests if the method append works correctly
	 */
	@Test
	public void appendValid() {
		Blockchain chain = new Blockchain(new Block(0, 0, 0L));
		Block headBlock = chain.getHeadBlock();
		//
		Block block = new Block(1, headBlock.hashCode(), 0L);
		chain.append(block);
		assertNotEquals(headBlock, chain.getHeadBlock());
		assertEquals(block, chain.getHeadBlock());
	}

	/**
	 * Tests if the method size returns the correct size.
	 */
	@Test
	public void verifySize() {
		Blockchain chain = new Blockchain(new Block(0, 0, 0L));

		assert chain.size() == 1;

		Block headBlock = chain.getHeadBlock();
		Block block = new Block(1, headBlock.hashCode(), 0L);
		chain.append(block);

		assert chain.size() == 2;
	}

	/**
	 * Verifies if the iterator work correctly
	 */
	@Test
	public void verifyIterator() {
		Blockchain<Transaction, BigDecimal> chain = new Blockchain<>(new Block<>(0, 0, BigDecimal.valueOf(0L)));
		Block<Transaction, BigDecimal> headBlock = chain.getHeadBlock();
		Block<Transaction, BigDecimal> block = new Block<>(1, headBlock.hashCode(), BigDecimal.valueOf(0L));
		chain.append(block);

		int i = 0;
		for (Block<Transaction, BigDecimal> b : chain) {
			if (i == 0) {
				assertEquals(b, headBlock);
			} else if (i == 1) {
				assertEquals(b, block);
			}
			i++;
		}
	}

	/**
	 * Tests if the method getTransactions() works correctly
	 */
	@Test
	public void getTransactionsValid() {
		// create a chain
		Blockchain chain = new Blockchain(new Block(0, 0, 0L));
		Block headBlock = chain.getHeadBlock();
		// create a block
		Block block = new Block(1, headBlock.hashCode(), 0L);

		// create a transaction and add it to the block, then add the block to the chain
		Address senderAddress = new UUIDAddress();
		Address receiverAddress = new UUIDAddress();
		List<Transaction> transactionsList = new Vector<>();
		transactionsList.add(new Transaction(senderAddress, receiverAddress, (double) 5, new Payload(0)));
		block.addTransactions(transactionsList);
		chain.append(block);

		// verify if the total number of txs is as expected
		int numberOfTxs = chain.getNumberOfTransactions();
		assertEquals(Integer.parseInt("1"), numberOfTxs);
	}

	/**
	 * Tests if the method contains(hashcode) works correctly
	 */
	@Test
	public void containsHashcodeValid() {
		// create a chain
		Blockchain chain = new Blockchain(new Block(0, 0, 0L));
		Block headBlock = chain.getHeadBlock();
		// create a block and add it to the chain
		Block block = new Block(1, headBlock.hashCode(), 0L);
		chain.append(block);
		// verify if the chain contains the block
		assertTrue(chain.contains(block.hashCode()));
	}

	/**
	 * Tests if the method contains(Block) works correctly
	 */
	@Test
	public void containsBlockValid() {
		// create a chain
		Blockchain chain = new Blockchain(new Block(0, 0, 0L));
		Block headBlock = chain.getHeadBlock();
		// create a block and add it to the chain
		Block block = new Block(1, headBlock.hashCode(), 0L);
		chain.append(block);
		// verify if the chain contains the block
		assertTrue(chain.contains(block));
	}

	/**
	 * Tests if the method contains(Block) works correctly
	 */
	@Test
	public void equals() {
		// create two chains
		Blockchain chain1 = new Blockchain(new Block(0, 0, 0L));
		Blockchain chain2 = new Blockchain(new Block(0, 0, 0L));
		Block headBlock1 = chain1.getHeadBlock();
		// create a block and add it to the chain
		Block block = new Block(1, headBlock1.hashCode(), 0L);
		chain1.append(block);
		assertFalse(chain1.equals(chain2));
		chain2.append(block);
		// verify if the chain contains the block
		assertTrue(chain1.equals(chain2));
	}
}
