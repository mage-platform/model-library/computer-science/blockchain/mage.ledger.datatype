/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package mage.ledger.datatype.blockchain;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNotSame;

import org.junit.jupiter.api.Test;

import mage.ledger.datatype.Address;
import mage.ledger.datatype.Payload;
import mage.ledger.datatype.Transaction;
import mage.ledger.datatype.UUIDAddress;
import mage.ledger.datatype.blockchain.MerkleTree;


/**
 * A MerkleTree (hash tree) is a collection in which every element is labelled
 * with the hash of that element. (see {@link #addHashedElement()})
 *
 * @author Önder Gürcan
 */
public class MerkleTreeTest {

    Address senderAddress = new UUIDAddress();
    Address receiverAddress = new UUIDAddress();

    @Test
    public void addHashedElement() {
        MerkleTree<Transaction> merkleTree = new MerkleTree<>();
        //
        Transaction transaction = new Transaction(senderAddress, receiverAddress, (double) 5, new Payload(0));
        merkleTree.addHashedElement(transaction);
        Transaction hashedElement = merkleTree.getHashedElement(transaction.hashCode());
        assertNotNull(hashedElement);
        assertEquals(transaction, hashedElement);
        //
        Transaction transaction2 = new Transaction(senderAddress, receiverAddress, (double) 7, new Payload(0));
        merkleTree.addHashedElement(transaction2);
        hashedElement = merkleTree.getHashedElement(transaction2.hashCode());
        assertNotNull(hashedElement);
        assertEquals(transaction2, hashedElement);
    }

    @Test
    public void copy() {
        MerkleTree<Transaction> merkleTree = new MerkleTree<>();
        //
        Transaction transaction = new Transaction(senderAddress, receiverAddress, (double) 5, new Payload(0));
        merkleTree.addHashedElement(transaction);

        MerkleTree<Transaction> copy = merkleTree.copy();

        assertNotSame(merkleTree, copy);
        assertEquals(merkleTree, copy);
    }

}
