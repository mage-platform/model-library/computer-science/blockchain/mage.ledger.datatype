/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package mage.ledger.datatype.blockchain;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNotSame;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.math.BigDecimal;
import java.util.List;
import java.util.Vector;

import org.junit.jupiter.api.Test;

import mage.ledger.datatype.Address;
import mage.ledger.datatype.ExceptionMessages;
import mage.ledger.datatype.Payload;
import mage.ledger.datatype.Transaction;
import mage.ledger.datatype.UUIDAddress;

/**
 * A block has a block number, a previous block hash, a time stamp, a nonce and
 * a set of transactions.
 * <p>
 * The block number should be an integer higher than or equal to 1. (see
 * {@link #createValid()} and {@link #createInvalid()}) The previous block has
 * should be a non-negative integer value. (see {@link #createValid()} and
 * {@link #createInvalid()}) The timestamp should be a non-negative long value.
 * (see {@link #createValid()} and {@link #createInvalid()}) The transactions
 * can be added to the set of transactions in a block. (see
 * {@link #addTransactionsValid()} ()}) If a transaction without a sender
 * address is added to a block, it is considered a the coinbase transaction of
 * that block. Blocks can be exactly copied since each node stores a copy of
 * them.
 *
 * @author Önder Gürcan
 */
public class BlockTest {

	Address senderAddress = new UUIDAddress();
	Address receiverAddress = new UUIDAddress();

	@Test
	public void createValid() {
		try {
			// A block has a block number, a previous block hash and a time stamp
			Block<?, ?> block = new Block<>(1, 0, 0L);
			assertNotNull(block);
			assertEquals(Integer.valueOf(1), block.getBlockNumber());
			assertEquals(0, block.getPreviousBlockHashCode());
			assertEquals(0L, block.getTimeStamp());
		} catch (Exception e) {
			fail(ExceptionMessages.THIS_LINE_SHOULD_NOT_BE_EXECUTED);
		}
	}

	@Test
	public void createInvalid() {
		try {
			new Block<>(-1, 0, 0L);
			fail(ExceptionMessages.THIS_LINE_SHOULD_NOT_BE_EXECUTED);
		} catch (Exception e) {
			assertTrue(e instanceof IllegalArgumentException);
			assertEquals(ExceptionMessages.BLOCK_NUMBER_SHOULD_BE_HIGHER_THAN_OR_EQUAL_TO_ZERO, e.getMessage());
		}

		// try {
		// new Block(1, -1, 0L);
		// fail(ExceptionMessages.THIS_LINE_SHOULD_NOT_BE_EXECUTED);
		// } catch (Exception e) {
		// assertTrue(e instanceof IllegalArgumentException);
		// assertTrue(e.getMessage().equals(ExceptionMessages.INVALID_HASH_VALUE));
		// }

		try {
			new Block<>(1, 0, -1L);
			fail(ExceptionMessages.THIS_LINE_SHOULD_NOT_BE_EXECUTED);
		} catch (Exception e) {
			assertTrue(e instanceof IllegalArgumentException);
			assertEquals(ExceptionMessages.TIME_STAMP_SHOULD_BE_HIGHER_THAN_OR_EQUAL_TO_ZERO, e.getMessage());
		}
	}

	@Test
	public void setNonceValid() {
		Block<?, ?> block = new Block<>(1, 0, 0L);
		block.setNonce(0);
		assertEquals(0, block.getNonce());
	}

	@Test
	public void setNonceInvalid() {
		try {
			Block<?, ?> block = new Block<>(1, 0, 0L);
			block.setNonce(-1);
			fail(ExceptionMessages.THIS_LINE_SHOULD_NOT_BE_EXECUTED);
		} catch (Exception e) {
			assertTrue(e instanceof IllegalArgumentException);
			assertEquals(ExceptionMessages.NONCE_SHOULD_BE_HIGHER_THAN_OR_EQUAL_TO_ZERO, e.getMessage());
		}
	}

	@SuppressWarnings("unchecked")
	@Test
	public void addTransactionsValid() {
		try {
			Block<Transaction, Long> block = new Block<>(1, 0, 0L);
			// TODO Block<P,T> should be parameterized
			//
			List<Transaction> transactionList = new Vector<Transaction>();

			Transaction transaction1 = new Transaction(senderAddress, receiverAddress, (double) 5, new Payload(0));
			transactionList.add(transaction1);
			Transaction transaction2 = new Transaction(senderAddress, receiverAddress, (double) 7, new Payload(0));
			transactionList.add(transaction2);
			Transaction transaction3 = new Transaction(senderAddress, receiverAddress, (double) 9, new Payload(0));
			transactionList.add(transaction3);
			//
			block.addTransactions(transactionList);
			//
			Transaction blockedTransaction = block.getTransaction(transaction1.hashCode());
			assertNotNull(blockedTransaction);
			assertEquals(transaction1, blockedTransaction);
			//
			blockedTransaction = block.getTransaction(transaction2.hashCode());
			assertNotNull(blockedTransaction);
			assertEquals(transaction2, blockedTransaction);
			//
			blockedTransaction = block.getTransaction(transaction3.hashCode());
			assertNotNull(blockedTransaction);
			assertEquals(transaction3, blockedTransaction);
		} catch (Exception e) {
			fail(ExceptionMessages.THIS_LINE_SHOULD_NOT_BE_EXECUTED);
		}
	}

	@Test
	public void addCoinbaseTransactionValid() {
		try {
			Block<Transaction, ?> block = new Block<>(1, 0, 0L);
			//
			Transaction coinbaseTransaction = block.getCoinbaseTransaction();
			assertNull(coinbaseTransaction);
			//
			List<Transaction> transactionList = new Vector<Transaction>();
			//
			Transaction transaction1 = new Transaction(senderAddress, receiverAddress, (double) 5, new Payload(0));
			transactionList.add(transaction1);
			Transaction transaction2 = new Transaction(null, receiverAddress, (double) 7, new Payload(0));
			transactionList.add(transaction2);

			// add the list of transactions to a block
			block.addTransactions(transactionList);

			// get the coinbase transaction in the block
			coinbaseTransaction = block.getCoinbaseTransaction();

			// coinbaseTransaction should be null since addTransactions() processes simple
			// transactions only (with not null sender and receiver)
			assertNull(coinbaseTransaction);

			// add the coinbase transaction to the block
			block.addCoinbaseTransaction(transaction2);
			coinbaseTransaction = block.getCoinbaseTransaction();
			// coinbaseTransaction should not be null since the coinbase transaction was
			// added to the block
			assertNotNull(coinbaseTransaction);
			assertEquals(transaction2, coinbaseTransaction);
		} catch (Exception e) {
			fail(ExceptionMessages.THIS_LINE_SHOULD_NOT_BE_EXECUTED);
		}
	}

	@Test
	public void getTotalFee() {
		Block<Transaction, ?> block = new Block<>(1, 0, 0L);
		//
		List<Transaction> transactionList = new Vector<>();
		//
		Transaction tx1 = new Transaction(senderAddress, receiverAddress, (double) 5, new Payload(0));
		transactionList.add(tx1);
		Transaction tx2 = new Transaction(senderAddress, receiverAddress, (double) 7, new Payload(0));
		transactionList.add(tx2);
		Transaction tx3 = new Transaction(senderAddress, receiverAddress, (double) 9, new Payload(0));
		transactionList.add(tx3);
		Transaction coinbaseTransaction = new Transaction(null, receiverAddress, (double) 12, new Payload(0));
		//
		assertEquals(0.0, block.getTotalFee(), 0.0);
		//
		block.addTransactions(transactionList);
		block.addCoinbaseTransaction(coinbaseTransaction);
		//
		assertEquals(33.0, block.getTotalFee(), 0.0);
	}

	/**
	 * Verify if a block can only add a specify type of transaction.
	 */
	@Test
	public void getGenericTransaction() {
		Block<TransactionLong, Long> block = new Block<>(0, 0, 0L);

		List<TransactionLong> listTx = new Vector<>();
		listTx.add(new TransactionLong(senderAddress, receiverAddress, 0.d, 42L));

		assert true;

		block.addTransactions(listTx);
		assert true;
	}

	@Test
	public void copy() {
		Block<Transaction, ?> block = new Block<>(1, 0, 0L);
		//
		List<Transaction> transactionList = new Vector<>();
		//
		Transaction transaction1 = new Transaction(senderAddress, receiverAddress, (double) 5, new Payload(0));
		transactionList.add(transaction1);
		Transaction transaction2 = new Transaction(senderAddress, receiverAddress, (double) 7, new Payload(0));
		transactionList.add(transaction2);
		Transaction transaction3 = new Transaction(senderAddress, receiverAddress, (double) 9, new Payload(0));
		transactionList.add(transaction3);
		Transaction coinbaseTransaction = new Transaction(null, receiverAddress, (double) 12, new Payload(0));
		transactionList.add(coinbaseTransaction);
		//
		block.addTransactions(transactionList);
		//
		Block<Transaction, ?> copy = block.copy();

		assertNotSame(block, copy);
		assertEquals(block, copy);
	}

	@Test
	public void hashcode() {
		// Same Block
		Block<Transaction, ?> blockA = new Block<>(1, 0, 0L);
		Block<Transaction, ?> blockB = new Block<>(1, 0, 0L);

		assertEquals(blockA.hashCode(), blockB.hashCode());

		// Different Block Numbers
		Block<Transaction, ?> block1 = new Block<>(1, 0, 0L);
		Block<Transaction, ?> block2 = new Block<>(2, 0, 0L);

		assertNotEquals(block2.hashCode(), block1.hashCode());

		// Different TimeStamps
		Block<Transaction, ?> blockTime1 = new Block<>(1, 0, 0L);
		Block<Transaction, ?> blockTime2 = new Block<>(1, 0, 1L);

		assertNotEquals(blockTime2.hashCode(), blockTime1.hashCode());

		// Different nonce
		Block<Transaction, ?> blockNonce1 = new Block<>(1, 0, 0L);
		blockNonce1.setNonce(10);

		Block<Transaction, ?> blockNonce2 = new Block<>(1, 0, 0L);
		blockNonce2.setNonce(20);

		assertNotEquals(blockNonce2.hashCode(), blockNonce1.hashCode());

		// Different previous block hashcodes
		Block<Transaction, ?> blockPrev1 = new Block<>(1, 0, 0L);
		Block<Transaction, ?> blockPrev2 = new Block<>(1, 2, 0L);
		assertNotEquals(blockPrev2.hashCode(), blockPrev1.hashCode());

		// Different Transactions
		Block<Transaction, ?> blockTxs1 = new Block<>(1, 0, 0L);
		List<Transaction> transactionList = new Vector<>();
		Transaction transaction1 = new Transaction(senderAddress, receiverAddress, (double) 4, new Payload(0));
		transactionList.add(transaction1);
		blockTxs1.addTransactions(transactionList);

		Block<?, ?> blockTxs2 = new Block<>(1, 0, 0L);

		assertNotEquals(blockTxs2.hashCode(), blockTxs1.hashCode());

	}

	private static class TransactionLong extends Transaction {

		// Constructors.

		public TransactionLong(Address senderAddress, Address receiverAddress, Double fee, Long payload) {
			super(senderAddress, receiverAddress, fee, new Payload(payload));
		}

		public TransactionLong(Address senderAddress, Address receiverAddress, Double fee, Long payload,
				BigDecimal lockTime) {
			super(senderAddress, receiverAddress, fee, new Payload(payload), lockTime);
		}
	}
}
