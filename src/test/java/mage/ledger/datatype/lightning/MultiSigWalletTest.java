/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package mage.ledger.datatype.lightning;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;

import mage.ledger.datatype.Address;
import mage.ledger.datatype.UUIDAddress;
import mage.ledger.datatype.lightning.MultiSigWallet;


/**
 * 
 * @author Önder Gürcan
 * @author Mohamed Aimen Djari
 *
 */
public class MultiSigWalletTest {

	/**
	 * {@link MultiSigWallet#MultiSigWallet(max.datatype.com.Address, max.datatype.com.Address)}
	 */
	@Test
	public void createWallet() {
		// Create two addresses
		Address address1 = new UUIDAddress();
		Address address2 = new UUIDAddress();

		// Create a wallet
		MultiSigWallet wallet = new MultiSigWallet(address1, address2);

		assertNotNull(wallet);
		assertEquals(Double.valueOf(0), wallet.getBalanceOf(address1));
		assertEquals(Double.valueOf(0), wallet.getBalanceOf(address2));
	}

	/**
	 * Tests {@link MultiSigWallet#setBalanceOf(String, Double)}
	 */
	@Test
	public void setBalances() {
		// Create two addresses
		Address address1 = new UUIDAddress();
		Double payload1 = Double.valueOf(10);

		Address address2 = new UUIDAddress();
		Double payload2 = Double.valueOf(100);

		// Create a wallet
		MultiSigWallet wallet = new MultiSigWallet(address1, address2);

		// Make sure the wallet has been created, set the balances to a certain payload
		// and check it
		wallet.setBalanceOf(address1.toString(), payload1);
		wallet.setBalanceOf(address2.toString(), payload2);

		assertEquals(payload1, wallet.getBalanceOf(address1));
		assertEquals(payload2, wallet.getBalanceOf(address2));
	}

	/**
	 * Tests
	 * {@link MultiSigWallet#sendLightningTransaction(Address, Address, Double)}
	 */
	@Test
	public void sendLightningTransaction() {
		// Create two addresses
		Address address1 = new UUIDAddress();
		Double payload1 = Double.valueOf(10);

		Address address2 = new UUIDAddress();
		Double payload2 = Double.valueOf(100);

		// Create a wallet
		MultiSigWallet wallet = new MultiSigWallet(address1, address2);

		Double payloadToSend = Double.valueOf(50);

		// Make sure the wallet has been created, set the balances to a certain payload,
		// send a transaction and check it
		wallet.setBalanceOf(address1.toString(), payload1);
		wallet.setBalanceOf(address2.toString(), payload2);
		wallet.sendLightningTransaction(address2, address1, payloadToSend);
		Double newPayload1 = payload1 + payloadToSend;
		Double newPayload2 = payload2 - payloadToSend;

		assertEquals(newPayload1, wallet.getBalanceOf(address1));
		assertEquals(newPayload2, wallet.getBalanceOf(address2));
	}
}
