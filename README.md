# Ledger Data Types

This project contains the data types commonly used in distributed ledger technologies (aka blockchain) such as
`Transaction`, `Block`, `Blockchain`, `MerkleTree` and `Wallet`. 

## Getting Started

To include this project in your application, add the following in your `pom.xml` file:

```xml
<!-- Add the MAGE platform group package repository. -->
<repositories>
  <repository>
    <id>gitlab-maven</id>
    <url>https://gitlab.com/api/v4/groups/16222017/-/packages/maven</url>
  </repository>
</repositories>

...

<!-- Add the MAGE Ledger Data Types project dependency. -->
<dependencies>
    <dependency>
      <groupId>mage-platform</groupId>
      <artifactId>mage.ledger.datatype</artifactId>
      <version>${latest.version}</version>
    </dependency>
</dependencies>
```

## Licence

This project is released under the [Eclipse Public Licence 2.0 (EPL 2.0)](https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html)

## Acknowledgment

This is a fork of the project that can be found [here](https://gitlab.com/cea-licia/max/datatypes/max.datatype.ledger).